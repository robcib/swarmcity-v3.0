﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Excel;
using System.IO;
using System.Data;
using System.Globalization;

public class CityClimate
{
    public double Tmed, Tmin, Tmax, rain;

    public CityClimate()
    {
        Tmed = 0;
        Tmin = 0;
        Tmax = 0;
        rain = 0;
    }

    public double GetTemperature(Vector3 p, int h, bool anomaly, Vector3 ap)
    {
        double d = Vector3.Distance(p, new Vector3(0, 0, 0));

        if (anomaly && (Vector3.Distance(ap, p) < 200))
        {
            double T = (Tmed + (Tmax - Tmin) / 2 * Mathf.Cos(Mathf.PI / 12 * (h - 4)) - d / 1000) * Random.Range(1.49f, 1.51f);

            //Debug.Log("Temperature: " + T);

            return T;
        }
        else
        {
            double T = (Tmed + (Tmax - Tmin) / 2 * Mathf.Cos(Mathf.PI / 12 * (h - 4)) - d / 1000) * Random.Range(0.99f, 1.01f);

            //Debug.Log("Temperature: " + T);

            return T;
        }
    }
}

public class PointPollution
{
    public double[] sulfur_oxides, carbon_oxides, nitrogen_oxides, particles;

    public PointPollution()
    {
        sulfur_oxides = new double[24] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        carbon_oxides = new double[24] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        nitrogen_oxides = new double[24] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        particles = new double[24] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    }
}

public class CityPollution
{
    public PointPollution p1pollution, p2pollution, p3pollution, p4pollution, p5pollution;
    public Vector3 p1position, p2position, p3position, p4position, p5position;

    public CityPollution()
    {
        p1pollution = new PointPollution();
        p2pollution = new PointPollution();
        p3pollution = new PointPollution();
        p4pollution = new PointPollution();
        p5pollution = new PointPollution();

        p1position = new Vector3(-50.3f, 10f, 256.8f);
        p2position = new Vector3(-17.3f, 10f, -48.1f);
        p3position = new Vector3(-172f, 10f, -18f);
        p4position = new Vector3(162.5f, 10f, -6.4f);
        p5position = new Vector3(32f, 10f, -200f);
    }

    public double GetSulfurOxides(Vector3 p, int h)
    {
        double v1 = p1pollution.sulfur_oxides[h];
        double v2 = p2pollution.sulfur_oxides[h];
        double v3 = p3pollution.sulfur_oxides[h];
        double v4 = p4pollution.sulfur_oxides[h];
        double v5 = p5pollution.sulfur_oxides[h];

        double d1 = 1 / Vector3.Distance(p1position, p);
        double d2 = 1 / Vector3.Distance(p2position, p);
        double d3 = 1 / Vector3.Distance(p3position, p);
        double d4 = 1 / Vector3.Distance(p4position, p);
        double d5 = 1 / Vector3.Distance(p5position, p);

        return (d1 * v1 + d2 * v2 + d3 * v3 + d4 * v4 + d5 * v5) / (d1 + d2 + d3 + d4 + d5) * Random.Range(0.95f, 1.05f);
    }
    
    public double GetCarbonOxides(Vector3 p, int h)
    {
        double v1 = p1pollution.carbon_oxides[h];
        double v2 = p2pollution.carbon_oxides[h];
        double v3 = p3pollution.carbon_oxides[h];
        double v4 = p4pollution.carbon_oxides[h];
        double v5 = p5pollution.carbon_oxides[h];

        double d1 = 1 / Vector3.Distance(p1position, p);
        double d2 = 1 / Vector3.Distance(p2position, p);
        double d3 = 1 / Vector3.Distance(p3position, p);
        double d4 = 1 / Vector3.Distance(p4position, p);
        double d5 = 1 / Vector3.Distance(p5position, p);

        return (d1 * v1 + d2 * v2 + d3 * v3 + d4 * v4 + d5 * v5) / (d1 + d2 + d3 + d4 + d5) * Random.Range(0.95f, 1.05f);
    }

    public double GetNitrogenOxides(Vector3 p, int h)
    {
        double v1 = p1pollution.nitrogen_oxides[h];
        double v2 = p2pollution.nitrogen_oxides[h];
        double v3 = p3pollution.nitrogen_oxides[h];
        double v4 = p4pollution.nitrogen_oxides[h];
        double v5 = p5pollution.nitrogen_oxides[h];

        double d1 = 1 / Vector3.Distance(p1position, p);
        double d2 = 1 / Vector3.Distance(p2position, p);
        double d3 = 1 / Vector3.Distance(p3position, p);
        double d4 = 1 / Vector3.Distance(p4position, p);
        double d5 = 1 / Vector3.Distance(p5position, p);

        return (d1 * v1 + d2 * v2 + d3 * v3 + d4 * v4 + d5 * v5) / (d1 + d2 + d3 + d4 + d5) * Random.Range(0.95f, 1.05f);
    }
    
    public double GetParticles(Vector3 p, int h, bool anomaly, Vector3 ap)
    {
        double v1 = p1pollution.particles[h];
        double v2 = p2pollution.particles[h];
        double v3 = p3pollution.particles[h];
        double v4 = p4pollution.particles[h];
        double v5 = p5pollution.particles[h];

        double d1 = 1 / Vector3.Distance(p1position, p);
        double d2 = 1 / Vector3.Distance(p2position, p);
        double d3 = 1 / Vector3.Distance(p3position, p);
        double d4 = 1 / Vector3.Distance(p4position, p);
        double d5 = 1 / Vector3.Distance(p5position, p);

        if (anomaly)
        {
            double d6 = 1 / Vector3.Distance(ap, p);
            double v6 = Mathf.Max(new float[5] { (float)d1, (float)d2, (float)d3, (float)d4, (float)d5 }) * 2;

            return (d1 * v1 + d2 * v2 + d3 * v3 + d4 * v4 + d5 * v5 + d6 * v6) / (d1 + d2 + d3 + d4 + d5 + d6) * Random.Range(1.49f, 1.51f);
        }
        else
        {
            return (d1 * v1 + d2 * v2 + d3 * v3 + d4 * v4 + d5 * v5) / (d1 + d2 + d3 + d4 + d5) * Random.Range(0.99f, 1.01f);
        }
    }
}

public class ClimateManager : MonoBehaviour {

    public CityPollution pollution;
    public CityClimate climate;

    public float anomaly_time;
    private float anomaly_started = 0;
    public bool anomaly;
    public Vector3 anomaly_pose;

    public string pBook, cBook;
    private string WorkSheet;
    public Vector3 measure;
    public List<string[]> TableClimate = new List<string[]>();
    public List<string[]> TablePollution = new List<string[]>();
    int[] cMonth;
    int[] pMonth;

    public List<string[]> ClimateMonth = new List<string[]>();
    public List<string[]> PollutionMonth = new List<string[]>();

    //private FileStream stream;
    //DataSet pDataSet, cDataSet;
    //private DataTable table, cTable;

    private TimeGenerator tg;

    private int day;
    private int hour;
    public int month;
    public int startMonth;
    int i;

    public CultureInfo culture = new CultureInfo("es-ES");

    private bool start;

    private double TempMeasured;
    private double SulfurOxidMeasured;
    private double CarbonOxidMeasured;
    private double NitrogenOxidMeasured;
    private double ParticleMeasured;

    // Use this for initialization
    void Start () {
        cMonth = new int[13] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        pMonth = new int[13] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        for (i = 1; i <= 12; i++)
        {
            readCSVfile(Application.streamingAssetsPath + "/Climate/" + pBook + i + ".csv", 1);
            pMonth[i] = TablePollution.Count;
            readCSVfile(Application.streamingAssetsPath + "/Climate/" + cBook + i + ".csv", 2);
            cMonth[i] = TableClimate.Count;
        }
        
        tg = GameObject.Find("Time").GetComponent<TimeGenerator>();

        pollution = new CityPollution();

        climate = new CityClimate();

        /*cTable = readXLS(Application.dataPath + "/Climate.xls");

        //stream = File.Open(Application.streamingAssetsPath + "/Climate/" + pBook, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

        //Debug.Log(Application.streamingAssetsPath + "/Climate/" + pBook);

        //IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);


        //Debug.Log("excelReader " + excelReader);

        //pDataSet = excelReader.AsDataSet();

        //Debug.Log("pDataSet " + pDataSet.ToString());

        stream = File.Open(Application.streamingAssetsPath + "/Climate/" + cBook, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

        IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

        cDataSet = excelReader.AsDataSet();
        */
        day = tg.date.Day;
        start = true;
        month = 0;

        anomaly = false;

    }
	
	// Update is called once per frame
	void Update () {

        if ((day != tg.date.Day)||(start == true))
        {
            start = false;
            if(tg.date.Month != month)
            {
                //Debug.Log(tg.date.Month);
                ClimateMonth = new List<string[]>();
                PollutionMonth = new List<string[]>();
                month = tg.date.Month;
                startMonth = month - 1;
                
                for(i = pMonth[startMonth]; i < pMonth[month]; i++)
                {
                    PollutionMonth.Add(TablePollution[i]);
                }
                for (i = cMonth[startMonth]; i < cMonth[month]; i++)
                {
                    ClimateMonth.Add(TableClimate[i]);
                }
            }
            /*switch (tg.date.Month)
            {
                case 1:
                    WorkSheet = "Jan";
                    break;
                case 2:
                    WorkSheet = "Feb";
                    break;
                case 3:
                    WorkSheet = "Mar";
                    break;
                case 4:
                    WorkSheet = "Apr";
                    break;
                case 5:
                    WorkSheet = "May";
                    break;
                case 6:
                    WorkSheet = "Jun";
                    break;
                case 7:
                    WorkSheet = "Jul";
                    break;
                case 8:
                    WorkSheet = "Aug";
                    break;
                case 9:
                    WorkSheet = "Sep";
                    break;
                case 10:
                    WorkSheet = "Oct";
                    break;
                case 11:
                    WorkSheet = "Nov";
                    break;
                case 12:
                    WorkSheet = "Dec";
                    break;
                default:
                    WorkSheet = "Jan";
                    break;
            }
            */

         
            //table = pDataSet.Tables[WorkSheet];

            for (int i = 1; i < PollutionMonth.Count; i++)
            {
                if (System.Convert.ToInt32(PollutionMonth[i][4]) == tg.date.Day)
                {
                    if (System.Convert.ToInt32(PollutionMonth[i][0]) == 1)
                    {
                        // Point 1:
                        if (System.Convert.ToInt32(PollutionMonth[i][1]) == 1)
                        {
                            // Sulfur Oxides:
                            for (int j = 5; j < PollutionMonth[0].Length; j++)
                            {
                                pollution.p1pollution.sulfur_oxides[j - 5] = System.Convert.ToDouble(PollutionMonth[i][j], culture);
                            }
                        }
                        else if (System.Convert.ToInt32(PollutionMonth[i][1]) == 6)
                        {
                            // Carbon Oxides:
                            for (int j = 5; j < PollutionMonth[0].Length; j++)
                            {
                                pollution.p1pollution.carbon_oxides[j - 5] = System.Convert.ToDouble(PollutionMonth[i][j], culture);
                            }
                        }
                        else if (System.Convert.ToInt32(PollutionMonth[i][1]) == 7)
                        {
                            // Nitrogen Oxides:
                            for (int j = 5; j < PollutionMonth[0].Length; j++)
                            {
                                pollution.p1pollution.nitrogen_oxides[j - 5] = System.Convert.ToDouble(PollutionMonth[i][j], culture);
                            }
                        }
                        else if (System.Convert.ToInt32(PollutionMonth[i][1]) == 10)
                        {
                            // Particles:
                            for (int j = 5; j < PollutionMonth[0].Length; j++)
                            {
                                pollution.p1pollution.particles[j - 5] = System.Convert.ToDouble(PollutionMonth[i][j], culture);
                            }
                        }
                    }
                    else if (System.Convert.ToInt32(PollutionMonth[i][0]) == 2)
                    {
                        // Point 2:
                        if (System.Convert.ToInt32(PollutionMonth[i][1]) == 1)
                        {
                            // Sulfur Oxides:
                            for (int j = 5; j < PollutionMonth[0].Length; j++)
                            {
                                pollution.p2pollution.sulfur_oxides[j - 5] = System.Convert.ToDouble(PollutionMonth[i][j], culture);
                            }
                        }
                        else if (System.Convert.ToInt32(PollutionMonth[i][1]) == 6)
                        {
                            // Carbon Oxides:
                            for (int j = 5; j < PollutionMonth[0].Length; j++)
                            {
                                pollution.p2pollution.carbon_oxides[j - 5] = System.Convert.ToDouble(PollutionMonth[i][j], culture);
                            }
                        }
                        else if (System.Convert.ToInt32(PollutionMonth[i][1]) == 7)
                        {
                            // Nitrogen Oxides:
                            for (int j = 5; j < PollutionMonth[0].Length; j++)
                            {
                                pollution.p2pollution.nitrogen_oxides[j - 5] = System.Convert.ToDouble(PollutionMonth[i][j], culture);
                            }
                        }
                        else if (System.Convert.ToInt32(PollutionMonth[i][1]) == 10)
                        {
                            // Particles:
                            for (int j = 5; j < PollutionMonth[0].Length; j++)
                            {
                                pollution.p2pollution.particles[j - 5] = System.Convert.ToDouble(PollutionMonth[i][j], culture);
                            }
                        }
                    }
                    else if (System.Convert.ToInt32(PollutionMonth[i][0]) == 3)
                    {
                        // Point 3:
                        if (System.Convert.ToInt32(PollutionMonth[i][1]) == 1)
                        {
                            // Sulfur Oxides:
                            for (int j = 5; j < PollutionMonth[0].Length; j++)
                            {
                                pollution.p3pollution.sulfur_oxides[j - 5] = System.Convert.ToDouble(PollutionMonth[i][j], culture);
                            }
                        }
                        else if (System.Convert.ToInt32(PollutionMonth[i][1]) == 6)
                        {
                            // Carbon Oxides:
                            for (int j = 5; j < PollutionMonth[0].Length; j++)
                            {
                                pollution.p3pollution.carbon_oxides[j - 5] = System.Convert.ToDouble(PollutionMonth[i][j], culture);
                            }
                        }
                        else if (System.Convert.ToInt32(PollutionMonth[i][1]) == 7)
                        {
                            // Nitrogen Oxides:
                            for (int j = 5; j < PollutionMonth[0].Length; j++)
                            {
                                pollution.p3pollution.nitrogen_oxides[j - 5] = System.Convert.ToDouble(PollutionMonth[i][j], culture);
                            }
                        }
                        else if (System.Convert.ToInt32(PollutionMonth[i][1]) == 10)
                        {
                            // Particles:
                            for (int j = 5; j < PollutionMonth[0].Length; j++)
                            {
                                pollution.p3pollution.particles[j - 5] = System.Convert.ToDouble(PollutionMonth[i][j], culture);
                            }
                        }
                    }
                    else if (System.Convert.ToInt32(PollutionMonth[i][0]) == 4)
                    {
                        // Point 4:
                        if (System.Convert.ToInt32(PollutionMonth[i][1]) == 1)
                        {
                            // Sulfur Oxides:
                            for (int j = 5; j < PollutionMonth[0].Length; j++)
                            {
                                pollution.p4pollution.sulfur_oxides[j - 5] = System.Convert.ToDouble(PollutionMonth[i][j], culture);
                            }
                        }
                        else if (System.Convert.ToInt32(PollutionMonth[i][1]) == 6)
                        {
                            // Carbon Oxides:
                            for (int j = 5; j < PollutionMonth[0].Length; j++)
                            {
                                pollution.p4pollution.carbon_oxides[j - 5] = System.Convert.ToDouble(PollutionMonth[i][j], culture);
                            }
                        }
                        else if (System.Convert.ToInt32(PollutionMonth[i][1]) == 7)
                        {
                            // Nitrogen Oxides:
                            for (int j = 5; j < PollutionMonth[0].Length; j++)
                            {
                                pollution.p4pollution.nitrogen_oxides[j - 5] = System.Convert.ToDouble(PollutionMonth[i][j], culture);
                            }
                        }
                        else if (System.Convert.ToInt32(PollutionMonth[i][1]) == 10)
                        {
                            // Particles:
                            for (int j = 5; j < PollutionMonth[0].Length; j++)
                            {
                                pollution.p4pollution.particles[j - 5] = System.Convert.ToDouble(PollutionMonth[i][j], culture);
                            }
                        }
                    }
                    else if (System.Convert.ToInt32(PollutionMonth[i][0]) == 6)
                    {
                        // Point 5:
                        if (System.Convert.ToInt32(PollutionMonth[i][1]) == 1)
                        {
                            // Sulfur Oxides:
                            for (int j = 5; j < PollutionMonth[0].Length; j++)
                            {
                                pollution.p5pollution.sulfur_oxides[j - 5] = System.Convert.ToDouble(PollutionMonth[i][j], culture);
                            }
                        }
                        else if (System.Convert.ToInt32(PollutionMonth[i][1]) == 6)
                        {
                            // Carbon Oxides:
                            for (int j = 5; j < PollutionMonth[0].Length; j++)
                            {
                                pollution.p5pollution.carbon_oxides[j - 5] = System.Convert.ToDouble(PollutionMonth[i][j], culture);
                            }
                        }
                        else if (System.Convert.ToInt32(PollutionMonth[i][1]) == 7)
                        {
                            // Nitrogen Oxides:
                            for (int j = 5; j < PollutionMonth[0].Length; j++)
                            {
                                pollution.p5pollution.nitrogen_oxides[j - 5] = System.Convert.ToDouble(PollutionMonth[i][j], culture);
                            }
                        }
                        else if (System.Convert.ToInt32(PollutionMonth[i][1]) == 10)
                        {
                            // Particles:
                            for (int j = 5; j < PollutionMonth[0].Length; j++)
                            {
                                pollution.p5pollution.particles[j - 5] = System.Convert.ToDouble(PollutionMonth[i][j], culture);
                            }
                        }
                    }
                }
            }

            //cTable = cDataSet.Tables[WorkSheet];

            for (int i = 1; i < ClimateMonth[0].Length; i++)
            {
                if (System.Convert.ToInt32(ClimateMonth[i][1]) == tg.date.Day)
                {
                    climate.Tmed = System.Convert.ToDouble(ClimateMonth[i][2], culture);
                    climate.Tmax = System.Convert.ToDouble(ClimateMonth[i][3], culture);
                    climate.Tmin = System.Convert.ToDouble(ClimateMonth[i][4], culture);
                    climate.rain = System.Convert.ToDouble(ClimateMonth[i][5], culture);
                    
                    break;
                }
            }

            //Debug.Log("Pollution-Sulfur Oxide: " + pollution.p1pollution.sulfur_oxides[1]);
            //Debug.Log("Pollution-Carbon Oxide: " + pollution.p1pollution.carbon_oxides[1]);
            //Debug.Log("Pollution-Nitrogen Oxide: " + pollution.p1pollution.nitrogen_oxides[1]);
            //Debug.Log("Pollution-Particles: " + pollution.p1pollution.particles[1]);
            
            day = tg.date.Day;
        }

        if ((Time.realtimeSinceStartup - anomaly_started) > anomaly_time && anomaly == false)
        {
            anomaly = true;
            anomaly_started = Time.realtimeSinceStartup;
        }
        if (anomaly && (Time.realtimeSinceStartup - anomaly_started) > anomaly_time)
        {
            anomaly = false;
            anomaly_started = Time.realtimeSinceStartup;
        }   

        if (Input.GetKey(KeyCode.I) && measure != null)
        {
            hour = tg.date.Hour;
            //TempMeasured = climate.GetTemperature(measure, hour, anomaly);
            Debug.Log("Temperature: " + TempMeasured);
            SulfurOxidMeasured = pollution.GetSulfurOxides(measure, hour);
            Debug.Log("Pollution-Sulfur Oxide: " + SulfurOxidMeasured);
            CarbonOxidMeasured = pollution.GetCarbonOxides(measure, hour);
            Debug.Log("Pollution-Carbon Oxide: " + CarbonOxidMeasured);
            NitrogenOxidMeasured = pollution.GetNitrogenOxides(measure, hour);
            Debug.Log("Pollution-Nitrogen Oxide: " + NitrogenOxidMeasured);
            //ParticleMeasured = pollution.GetParticles(measure, hour, anomaly);
            Debug.Log("Pollution-Particles: " + ParticleMeasured);
        }

        if (Input.GetKey(KeyCode.T) && measure != null)
        {
            Debug.Log("Temperature Data (Tmed, Tmax, Tmin, rain)" + climate.Tmed + "," + climate.Tmax + "," + climate.Tmin + "," + climate.rain);
            hour = tg.date.Hour;
            //TempMeasured = climate.GetTemperature(measure, hour, anomaly);
            Debug.Log("Temperature: " + TempMeasured);
            
        }
        if (Input.GetKey(KeyCode.K))
        {
            Debug.Log("Numero en string " + ClimateMonth[1][2]);
            Debug.Log("Numero en double " + (System.Convert.ToDouble(ClimateMonth[1][2], culture)));

            //Debug.Log(cTable.Rows.Count);

        }
    }

 /*   public DataTable readXLS(string filetoread)
    {
        // Must be saved as excel 2003 workbook, not 2007, mono issue really
        string con = "Driver={Microsoft Excel Driver (*.xls)}; DriverId=790; Dbq=" + filetoread + ";";
        Debug.Log(con);
        string yourQuery = "SELECT * FROM [Sheet1$]";
        // our odbc connector 
        OdbcConnection oCon = new OdbcConnection(con);
        // our command object 
        OdbcCommand oCmd = new OdbcCommand(yourQuery, oCon);
        // table to hold the data 
        DataTable dtYourData = new DataTable("YourData");
        // open the connection 
        oCon.Open();
        // lets use a datareader to fill that table! 
        OdbcDataReader rData = oCmd.ExecuteReader();
        // now lets blast that into the table by sheer man power! 
        dtYourData.Load(rData);
        // close that reader! 
        rData.Close();
        // close your connection to the spreadsheet! 
        oCon.Close();
        // wow look at us go now! we are on a roll!!!!! 
        // lets now see if our table has the spreadsheet data in it, shall we? 

        if (dtYourData.Rows.Count > 0)
        {
            // do something with the data here 
            // but how do I do this you ask??? good question! 
            for (int i = 0; i < dtYourData.Rows.Count; i++)
            {
                // for giggles, lets see the column name then the data for that column! 
                Debug.Log(dtYourData.Columns[0].ColumnName + " : " + dtYourData.Rows[i][dtYourData.Columns[0].ColumnName].ToString() +
                    "  |  " + dtYourData.Columns[1].ColumnName + " : " + dtYourData.Rows[i][dtYourData.Columns[1].ColumnName].ToString() +
                    "  |  " + dtYourData.Columns[2].ColumnName + " : " + dtYourData.Rows[i][dtYourData.Columns[2].ColumnName].ToString());
            }
        }
        return dtYourData;
    }*/

    public void readCSVfile(string path, int type)
    {
        string[] newTable;
        
        StreamReader strReader = new StreamReader(path);                //Lee el archivo csv a partir de la ruta
        //Debug.Log(path);
        bool endOfFile = false;
        int i = 0;
        int j = 0;
        while (!endOfFile)
        {
            string data_String = strReader.ReadLine();                  //Lee linea a linea el archivo csv
            if(data_String == null)
            {
                endOfFile = true;
                break;
            }
            var data_values = data_String.Split(';');                   //Divide la linea según los ;
            if (data_values[1] == "")
            {
                endOfFile = true;
                break;
            }
                
            newTable = new string[data_values.Length];
            for (j=0; j< data_values.Length; j++)
            {
                newTable[j] = data_values[j];
            }

            if (type == 1)                                              //Según el tipo que sea asigna los valores a la tabla de polución o a la de clima
            {
                TablePollution.Add(newTable);
            }else if (type == 2)
            {
                TableClimate.Add(newTable);
            }
            
            i++;
            
        }
        //Debug.Log("dias de cada mes del clima" + TableClimate.Count);
    }
}
