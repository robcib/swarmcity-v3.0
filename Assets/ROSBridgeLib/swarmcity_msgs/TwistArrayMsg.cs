﻿using System.Collections;
using System.Text;
using SimpleJSON;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.geometry_msgs;
using UnityEngine;

/*
 * Define a swarmcity_msgs twistarray message.
 * 
 * @author Juan Jesús Roldán (jj.roldan@upm.es)
 */

namespace ROSBridgeLib {
	namespace swarmcity_msgs {
		public class TwistArrayMsg : ROSBridgeMsg {
            public TwistMsg[] _twists;

			public TwistArrayMsg(JSONNode msg) {
                _twists = new TwistMsg[msg["twists"].Count];

                for (int i = 0; i < _twists.Length; i++)
                {
                    _twists[i] = new TwistMsg(msg["twists"][i]);
                }
            }

            public TwistArrayMsg(TwistMsg[] twists)
            {
                _twists = twists;
            }
			
			public static string getMessageType() {
				return "swarmcity_msgs1/TwistArray";
			}

            public TwistMsg[] GetTwists() {
				return _twists;
			}
			
			public override string ToString() {

                string array = "[";
                for (int i = 0; i < _twists.Length; i++)
                {
                    array = array + _twists[i];
                    if (_twists.Length - i > 1)
                        array += ",";
                }
                array += "]";

				return "swarmcity_msgs1/TwistArray [twists=" + array + "]";
			}
			
			public override string ToYAMLString() {
                string array = "[";
                for (int i = 0; i < _twists.Length; i++)
                {
                    array = array + _twists[i].ToYAMLString();
                    if (_twists.Length - i > 1)
                        array += ",";
                }
                array += "]";
                return "{\"twists\" : " + array + "}";
			}
		}
	}
}
