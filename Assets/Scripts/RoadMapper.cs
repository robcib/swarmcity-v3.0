﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class RoadMapper : MonoBehaviour {

	public int X_min, X_max;

	public int Z_min, Z_max;

	public int Resolution;

	public byte[,] map;

	private Vector3 offset;

	// Use this for initialization
	void Start () {
		offset = new Vector3 (0, 0, 0);

		map = new byte[(Z_max - Z_min) + 1, (X_max - X_min) + 1];

		for (int j = Z_min; j <= Z_max; j += Resolution) {
			for (int i = X_min; i <= X_max; i += Resolution) {
				map [(j - Z_min), (i - X_min)] = byte.MinValue;
			}
		}

		foreach (Component road in this.GetComponentsInChildren<Component>()) {

			Debug.Log ("Object " + road.ToString ());

			Transform tr = road.GetComponentInChildren<Transform> ();

			int angle = (int)tr.rotation.y;
			int x = (int)tr.position.x - (int)offset.x - X_min;
			int z = (int)tr.position.z - (int)offset.z - Z_min;

			Debug.Log ("Transform " + tr.ToString ());

			TrafficSystemPiece tsp = road.GetComponentInChildren<TrafficSystemPiece> ();
		
			if (tsp != null) {

				Debug.Log ("TrafficSystemPiece " + tsp.ToString ());

				if (tsp.m_roadPieceType == TrafficSystemPiece.RoadPieceType.NORMAL) {
					for (int i = -7; i <= 7; i++) {
						for (int j = -7; j <= 7; j++) {

							//Debug.Log ("NORMAL " + tsp.ToString ());
							//Debug.Log ("Z: " + (z + j).ToString () + " X: " + (x + i).ToString ());

							if (((angle > -5) && (angle < 5)) || ((angle > 175) && (angle < 185)) || ((angle > -185) && (angle < -175))) {
								if ((z + j >= 0)&&(z + j < Z_max - Z_min + 1)&&(x + i >= 0)&&(x + i < X_max - X_min + 1)){
									map [z + j, x + i] = byte.MaxValue;
								}
							} else if (((angle > 85) && (angle < 95)) || ((angle > 265) && (angle < 275)) || ((angle > -95) && (angle < -85))) {
								if ((z + i >= 0)&&(z + i < Z_max - Z_min + 1)&&(x + j >= 0)&&(x + j < X_max - X_min + 1)){
									map [z + i, x + j] = byte.MaxValue;
								}
							}
						}
					}
				}

				if (tsp.m_roadPieceType == TrafficSystemPiece.RoadPieceType.ROUNDABOUT) {
					for (int i = -32; i <= 32; i++) {
						for (int j = -7; j <= 7; j++) {
							if ((z + j >= 0) && (z + j < Z_max - Z_min + 1) && (x + i >= 0) && (x + i < X_max - X_min + 1)) {
								map [z + j, x + i] = byte.MaxValue;
							}
							if ((z + i >= 0) && (z + i < Z_max - Z_min + 1) && (x + j >= 0) && (x + j < X_max - X_min + 1)) {
								map [z + i, x + j] = byte.MaxValue;
							}
						}
					}
				}

				if (tsp.m_roadPieceType == TrafficSystemPiece.RoadPieceType.CROSS_INTERSECTION) {
					for (int i = -20; i <= 20; i++) {
						for (int j = -7; j <= 7; j++) {
							if ((z + j >= 0) && (z + j < Z_max - Z_min + 1) && (x + i >= 0) && (x + i < X_max - X_min + 1)) {
								map [z + j, x + i] = byte.MaxValue;
							}
							if ((z + i >= 0) && (z + i < Z_max - Z_min + 1) && (x + j >= 0) && (x + j < X_max - X_min + 1)) {
								map [z + i, x + j] = byte.MaxValue;
							}
						}
					}
				}

				if (tsp.m_roadPieceType == TrafficSystemPiece.RoadPieceType.T_INTERSECTION) {
					for (int i = -20; i <= 20; i++) {
						for (int j = -7; j <= 7; j++) {
							if ((z + j >= 0) && (z + j < Z_max - Z_min + 1) && (x + i >= 0) && (x + i < X_max - X_min + 1)) {
								map [z + j, x + i] = byte.MaxValue;
							}
							if ((z + i >= 0) && (z + i < Z_max - Z_min + 1) && (x + j >= 0) && (x + j < X_max - X_min + 1)) {
								map [z + i, x + j] = byte.MaxValue;
							}
						}
					}
				}
			}
		}

		Debug.Log ("Creating map...");

		var sr = File.CreateText ("Mapping.txt");

		sr.Write ("Map = [");
		for (int j = Z_min; j <= Z_max; j++){
			for (int i = X_min; i <= X_max; i++) {
				sr.Write (map [j - Z_min, i - X_min].ToString ());
				sr.Write (" ");
			}

			if (j != Z_max) {
				sr.Write ("; ");
			} else {
				sr.Write ("];");
			}
		}

		Debug.Log ("Map created!");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
