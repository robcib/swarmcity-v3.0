﻿using ROSBridgeLib;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.geometry_msgs;
using System.Collections;
using SimpleJSON;
using UnityEngine;

/*
 * Publisher of PoseArray message.
 * 
 * @author Juan Jesús Roldán (jj.roldan@upm.es)
 */

public class PoseArrayPub2 : ROSBridgePublisher {

	public new static string GetMessageTopic() {
		return "/Swarm/PoseArray2";
	}  

	public new static string GetMessageType() {
		return "geometry_msgs/PoseArray";
	}

	public static string ToString(PoseArrayMsg msg) {
		return msg.ToString ();
	}

    public static string ToYAMLString(PoseArrayMsg msg) {
		return msg.ToYAMLString ();
	}
}
