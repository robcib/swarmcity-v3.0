﻿using System.Collections;
using System.Text;
using SimpleJSON;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.geometry_msgs;
using ROSBridgeLib.sensor_msgs;
using ROSBridgeLib.swarmcity_msgs;
using UnityEngine;

/*
 * Define a swarmcity_msgs Car message. This has been hand-crafted from the corresponding
 * geometry_msgs message file.
 * 
 * @author Juan Jesús Roldán (jj.roldan@upm.es)
 */

namespace ROSBridgeLib {
	namespace swarmcity_msgs {
		public class CarArrayMsg : ROSBridgeMsg {

            public CarMsg[] _cars;

            public CarArrayMsg(JSONNode msg) {

                _cars = new CarMsg[msg["cars"].Count];

                for (int i = 0; i < _cars.Length; i++)
                {
                    _cars[i] = new CarMsg(msg["cars"][i]);
                }
            }

            public CarArrayMsg(CarMsg[] cars)
            {
                _cars = cars;
            }
			
			public static string getMessageType() {
				return "swarmcity_msgs2/CarArray";
			}
			
			public override string ToString() {
                string array = "[";
                for (int i = 0; i < _cars.Length; i++)
                {
                    array = array + "\"" + _cars[i].ToString() + "\"";
                    if (_cars.Length - i > 1)
                        array += ",";
                }
                array += "]";

                return "swarmcity_msgs2/CarArray [cars=\"" + array + "]";
			}
			
			public override string ToYAMLString() {
                string array = "[";
                for (int i = 0; i < _cars.Length; i++)
                {
                    array = array + _cars[i].ToYAMLString();
                    if (_cars.Length - i > 1)
                        array += ",";
                }
                array += "]";

                return "{\"cars\" : " + array + "}";
			}
		}
	}
}
