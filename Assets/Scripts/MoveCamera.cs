﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour {

	public float speed = 5.0f;
    public Camera cam;
    public Camera flyCam;
    public GameObject player;

    int mode;

	// Use this for initialization
	void Start () {
        mode = 0;
	}
	
	// Update is called once per frame
	void Update()
	{
        if (Input.GetKeyDown(KeyCode.Space))
        {
            switch(mode)
            {
                case 0:
                    cam.enabled = false;
                    flyCam.enabled = false;
                    player.transform.localScale = new Vector3(100, 100, 100);
                    mode = 1;
                    break;
                case 1:
                    cam.enabled = true;
                    flyCam.enabled = false;
                    player.transform.localScale = new Vector3(1, 1, 1);
                    mode = 2;
                    break;
                case 2:
                    cam.enabled = false;
                    flyCam.enabled = true;
                    player.transform.localScale = new Vector3(1, 1, 1);
                    mode = 3;
                    break;
                case 3:
                    cam.enabled = false;
                    flyCam.enabled = false;
                    player.transform.localScale = new Vector3(1, 1, 1);
                    mode = 0;
                    break;
            }

        }

		/*if((Input.GetKey(KeyCode.RightArrow))||(Input.GetKey(KeyCode.D)))
		{
			transform.position = transform.position + new Vector3(speed * Time.deltaTime,0,0);
		}
		if((Input.GetKey(KeyCode.LeftArrow))||(Input.GetKey(KeyCode.A)))
		{
			transform.position = transform.position - new Vector3(speed * Time.deltaTime,0,0);
		}
		if((Input.GetKey(KeyCode.DownArrow))||(Input.GetKey(KeyCode.S)))
		{
			transform.position = transform.position - new Vector3(0, 0, speed * Time.deltaTime);
		}
		if((Input.GetKey(KeyCode.UpArrow))||(Input.GetKey(KeyCode.W)))
		{
			transform.position = transform.position + new Vector3(0, 0, speed * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.Q)) 
		{
			transform.RotateAround (transform.position, new Vector3 (0, 1, 0), -speed * Time.deltaTime);			
		}
		if (Input.GetKey(KeyCode.E)) 
		{
			transform.RotateAround (transform.position, new Vector3 (0, 1, 0), speed * Time.deltaTime);
		}*/
	}
}
