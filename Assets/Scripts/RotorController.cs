﻿using UnityEngine;
using System.Collections;

public class RotorController : MonoBehaviour 
{
	public GameObject player;
	public GameObject rotor;
	public int rot;
	public float speed;
	
	private Vector3 offset;
	private float angle;
	
	void Start () 
	{
		offset = transform.position - player.transform.position;
	}
	
	void Update () 
	{
		Vector3 pose = Quaternion.Euler(player.transform.rotation.eulerAngles) * offset;
		
		rotor.transform.position = player.transform.position + pose;
		
		rotor.transform.rotation = player.transform.rotation;
		
		if (rot == 0) 
		{
			angle += 10;
			if (angle > 360)
			{
				angle = 0;
			}
			
			Vector3 rotation = new Vector3 (0, angle, 0);
			
			rotor.transform.Rotate (rotation);
		} 
		else 
		{
			angle -= 10;
			if (angle < 0)
			{
				angle = 360;
			}
			
			Vector3 rotation = new Vector3 (0, angle, 0);
			
			rotor.transform.Rotate (rotation);
		}
	}
}
