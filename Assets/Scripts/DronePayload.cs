﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

public class LaserScan
{
    public float _angle_min;
    public float _angle_max;
    public float _angle_increment;
    public float _time_increment;
    public float _scan_time;
    public float _range_min;
    public float _range_max;
    public float[] _ranges;
    public float[] _intensities;

    public LaserScan() 
    {
        _angle_min = -Mathf.PI;
        _angle_max = Mathf.PI;
        _angle_increment = Mathf.PI / 4;
        _time_increment = 0.0f;
        _scan_time = 0.1f;
        _range_min = 0.1f;
        _range_max = 20.0f;

        int size = (int)((_angle_max - _angle_min) / _angle_increment);

        _ranges = new float[8]{0, 0, 0, 0, 0, 0, 0, 0};
        _intensities = new float[8]{0, 0, 0, 0, 0, 0, 0, 0};
    }
}

public class Detection
{
    public string _type;
    public string[] _features;
    public Vector3 _position;
    public float _angle;

    public Detection(string type, string[] features, Vector3 position, float angle)
    {
        _type = type;
        _features = features;
        _position = position;
        _angle = angle;
    }
}

public class DronePayload : MonoBehaviour {

    public TrafficSystem trafficSystem;

    public int frequency;

    public int mode;

    public LaserScan laserscan;

    public List<Detection> detections;

    public double distance;
    public double probability;

    private Vector2 dronePose;
    private Vector2 carPose;
    private float carOri;
    private Vector2 popPose;
    private Vector2 perPose;
    private float perOri;

    private Detection detection;

    private bool det;

    private ClimateManager climate;

    public double carbon_oxides, sulfur_oxides, nitrogen_oxides, particles, temperature;

    private TimeGenerator time;

    private Stopwatch stopwatch;

	// Use this for initialization
	void Start () {
        mode = 1;

        laserscan = new LaserScan();

        trafficSystem = GameObject.Find("Traffic System").GetComponent<TrafficSystem>();

        climate = GameObject.Find("Climate").GetComponent<ClimateManager>();

        time = GameObject.Find("Time").GetComponent<TimeGenerator>();

        stopwatch = new Stopwatch();

        stopwatch.Start();
	}
	
	// Update is called once per frame
    void Update()
    {
        if (stopwatch.Elapsed.Milliseconds > frequency)
        {
            stopwatch.Stop();

            if (mode == 1)
            {
                distance = 0.5f * this.transform.position.y;

                if (this.transform.position.y > 100)
                {
                    probability = 0;
                }
                else if (this.transform.position.y < 20)
                {
                    probability = 1;
                }
                else
                {
                    probability = (100 - this.transform.position.y) / 80;
                }

                detections = new List<Detection>();

                DetectObstacles();

                DetectCars();

                DetectPeople();

                MeasurePollution();

                MeasureClimate();
            }

            stopwatch.Start();
        }
    }

    public void DetectObstacles()
    {
        RaycastHit hit;

        float angle;
        Vector3 direction;

        for (int i = 0; i < laserscan._ranges.Length; i++)
        {
            angle = laserscan._angle_min + i * laserscan._angle_increment + transform.rotation.eulerAngles.y * Mathf.PI / 180;

            direction = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle));

            //UnityEngine.Debug.Log(direction);

            hit = new RaycastHit();

            bool ray = Physics.Raycast(transform.position, direction, out hit);

            //UnityEngine.Debug.Log(hit.distance);

            if ((hit.distance > laserscan._range_min) && (hit.distance < laserscan._range_max))
            {
                laserscan._ranges[i] = hit.distance;
            }
            else
            {
                laserscan._ranges[i] = -1;
            }
        }

        //UnityEngine.Debug.Log("Ranges: [" + laserscan._ranges[0] + "," + laserscan._ranges[1] + "," + laserscan._ranges[2] + "," + laserscan._ranges[3] + "," + laserscan._ranges[4] + "," + laserscan._ranges[5] + "," + laserscan._ranges[6] + "," + laserscan._ranges[7] + "]");
    }

    public void MeasurePollution()
    {
        int hour = time.date.Hour;

        Vector3 pose = transform.position;

        sulfur_oxides = climate.pollution.GetSulfurOxides(pose, hour);

        carbon_oxides = climate.pollution.GetCarbonOxides(pose, hour);

        nitrogen_oxides = climate.pollution.GetNitrogenOxides(pose, hour);

        particles = climate.pollution.GetParticles(pose, hour, climate.anomaly, climate.anomaly_pose);
    } 

    public void MeasureClimate()
    {
        int hour = time.date.Hour;

        Vector3 pose = transform.position;

        temperature = climate.climate.GetTemperature(pose, hour, climate.anomaly, climate.anomaly_pose);
    }

    public void DetectPeople()
    {
        dronePose = new Vector2(transform.position.x, transform.position.z);

        det = false;

        foreach (GameObject pop in GameObject.FindGameObjectsWithTag("Population"))
        {
            //Debug.Log("Car: " + car.ToString());
            popPose = new Vector2(pop.transform.position.x, pop.transform.position.z);

            if ((Vector2.Distance(dronePose, popPose) < 20*distance) && (Random.value < probability))
            {
                for (int i = 0; i < pop.transform.childCount; i++)
                {
                    Transform child = pop.transform.GetChild(i);

                    if (child.tag == "Person")
                    {
                        perPose = new Vector2(child.transform.position.x, child.transform.position.z);
                        perOri = child.transform.rotation.eulerAngles.y;

                        if (Vector2.Distance(dronePose, perPose) < distance)
                        {
                            det = true;

                            switch ((child as Transform).gameObject.name)
                            {
                                case "NPC Woman 1(Clone)":
                                    //UnityEngine.Debug.Log("Person: Woman 1");
                                    detection = new Detection("person", new string[1] { "woman" }, perPose, perOri);
                                    break;
                                case "NPC Woman 2(Clone)":
                                    //UnityEngine.Debug.Log("Person: Woman 2");
                                    detection = new Detection("person", new string[1] { "woman" }, perPose, perOri);
                                    break;
                                case "NPC Woman 3(Clone)":
                                    //UnityEngine.Debug.Log("Person: Woman 3");
                                    detection = new Detection("person", new string[1] { "woman" }, perPose, perOri);
                                    break;
                                case "NPC Woman 4(Clone)":
                                    //UnityEngine.Debug.Log("Person: Woman 4");
                                    detection = new Detection("person", new string[1] { "woman" }, perPose, perOri);
                                    break;
                                case "NPC Man 1(Clone)":
                                    //UnityEngine.Debug.Log("Person: Man 1");
                                    detection = new Detection("person", new string[1] { "man" }, perPose, perOri);
                                    break;
                                case "NPC Man 2(Clone)":
                                    //UnityEngine.Debug.Log("Person: Man 2");
                                    detection = new Detection("person", new string[1] { "man" }, perPose, perOri);
                                    break;
                                case "NPC Man 3(Clone)":
                                    //UnityEngine.Debug.Log("Person: Man 3");
                                    detection = new Detection("person", new string[1] { "man" }, perPose, perOri);
                                    break;
                                case "NPC Man 4(Clone)":
                                    //UnityEngine.Debug.Log("Person: Man 4");
                                    detection = new Detection("person", new string[1] { "man" }, perPose, perOri);
                                    break;
                            }

                            detections.Add(detection);
                        }
                    }
                }
            }
        }

        if (det == false)
        {
            detection = new Detection("person:none", new string[1] { "none" }, new Vector2(0, 0), 0.0f);

            detections.Add(detection);
        }

        //UnityEngine.Debug.Log("Detection: " + detections.Count + " - " + detections[0]._type + detections[0]._features + detections[0]._position);
    }

    public void DetectCars()
    {
        dronePose = new Vector2(transform.position.x, transform.position.z);

        det = false;

        int index = 0;
        int detected = 0;

        TrafficManager tm = GameObject.FindObjectOfType<TrafficManager>();

        int total = tm.cars.Count;

        //foreach (TrafficSystemVehicle car in trafficSystem.GetSpawnedVehicles())
        foreach (TrafficSystemVehicle car in tm.cars)
        {
            if (car != null)
            {
                //UnityEngine.Debug.Log("Car: " + car.ToString());
                carPose = new Vector2(car.transform.position.x, car.transform.position.z);
                carOri = car.transform.rotation.eulerAngles.y;

                if ((Vector2.Distance(dronePose, carPose) < distance) && (Random.value < probability))
                {
                    det = true;
                    //UnityEngine.Debug.Log(car.name);
                    //UnityEngine.Debug.Log("Angle: " + carOri);

                    switch (car.name)
                    {
                        case "Car0(Clone)":
                            //Debug.Log("Drone " + drone.name + " detected red car");
                            detection = new Detection("car", new string[4] { index.ToString(), total.ToString(), "car", "red" }, carPose, carOri);
                            break;
                        case "Car1(Clone)":
                            //Debug.Log("Drone " + drone.name + " detected blue car");
                            detection = new Detection("car", new string[4] { index.ToString(), total.ToString(), "car", "blue" }, carPose, carOri);
                            break;
                        case "Car2(Clone)":
                            //Debug.Log("Drone " + drone.name + " detected green car");
                            detection = new Detection("car", new string[4] { index.ToString(), total.ToString(), "car", "green" }, carPose, carOri);
                            break;
                        case "Car3(Clone)":
                            //Debug.Log("Drone " + drone.name + " detected purple car");
                            detection = new Detection("car", new string[4] { index.ToString(), total.ToString(), "car", "purple" }, carPose, carOri);
                            break;
                        case "Car4(Clone)":
                            //Debug.Log("Drone " + drone.name + " detected cyan car");
                            detection = new Detection("car", new string[4] { index.ToString(), total.ToString(), "car", "cyan" }, carPose, carOri);
                            break;
                        case "Car5(Clone)":
                            //Debug.Log("Drone " + drone.name + " detected orange car");
                            detection = new Detection("car", new string[4] { index.ToString(), total.ToString(), "car", "orange" }, carPose, carOri);
                            break;
                        case "Car6(Clone)":
                            //Debug.Log("Drone " + drone.name + " detected gray car");
                            detection = new Detection("car", new string[4] { index.ToString(), total.ToString(), "car", "gray" }, carPose, carOri);
                            break;
                        case "Van1(Clone)":
                            //Debug.Log("Drone " + drone.name + " detected white van");
                            detection = new Detection("car", new string[4] { index.ToString(), total.ToString(), "van", "white" }, carPose, carOri);
                            break;
                        case "Van2(Clone)":
                            //Debug.Log("Drone " + drone.name + " detected green van");
                            detection = new Detection("car", new string[4] { index.ToString(), total.ToString(), "van", "green" }, carPose, carOri);
                            break;
                    }

                    detections.Add(detection);

                    //UnityEngine.Debug.Log("Detection: " + detection._type + " " + detection._features + " " + detection._position);
                }
            }
            index++;
            //UnityEngine.Debug.Log(index.ToString());
        }

        if (det == false)
        {
            detection = new Detection("car:none", new string[1] { total.ToString() }, dronePose, carOri);

            detections.Add(detection);
        }
                
        //GameObject canvas = GameObject.Find("Text");

        //Text text = canvas.GetComponent<Text>();

        //text.text = "Number of cars - " + total.ToString();

        //UnityEngine.Debug.Log("Detection: " + detections[0]._type + detections[0]._features + detections[0]._position);
	}
}
