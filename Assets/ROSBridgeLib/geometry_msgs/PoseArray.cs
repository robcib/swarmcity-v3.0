﻿using System.Collections;
using System.Text;
using SimpleJSON;
using ROSBridgeLib.std_msgs;

/*
 * Define a geometry_msgs posearray message. This has been hand-crafted from the corresponding
 * geometry_msgs message file.
 * 
 * @author Juan Jesús Roldán (jj.roldan@upm.es)
 */

namespace ROSBridgeLib {
	namespace geometry_msgs {
		public class PoseArrayMsg : ROSBridgeMsg {
            public HeaderMsg _header;
			public PoseMsg[] _poses;

			public PoseArrayMsg(JSONNode msg) {
                _header = new HeaderMsg(msg["header"]);
                _poses = new PoseMsg[msg["poses"].Count];

                for (int i = 0; i < _poses.Length; i++)
                {
                    _poses[i] = new PoseMsg(msg["poses"][i]);
                }
            }

			public PoseArrayMsg(HeaderMsg header, PoseMsg[] poses) {
                _header = header;
                _poses = poses;
            }
			
			public static string getMessageType() {
				return "geometry_msgs/PoseArray";
			}

            public PoseMsg[] GetPosition() {
				return _poses;
			}
			
			public override string ToString() {

                string array = "[";
                for (int i = 0; i < _poses.Length; i++)
                {
                    array = array + _poses[i];
                    if (_poses.Length - i > 1)
                        array += ",";
                }
                array += "]";

				return "geometry_msgs/PoseArray [header=" + _header.ToString() + ",  poses=" + array + "]";
			}
			
			public override string ToYAMLString() {
                string array = "[";
                for (int i = 0; i < _poses.Length; i++)
                {
                    array = array + _poses[i].ToYAMLString();
                    if (_poses.Length - i > 1)
                        array += ",";
                }
                array += "]";
                return "{\"header\" : " + _header.ToYAMLString() + ", \"poses\" : " + array + "}";
			}
		}
	}
}
