﻿using System.Collections;
using System.Text;
using SimpleJSON;
using ROSBridgeLib.std_msgs;
using UnityEngine;

/*
 * Define a geometry_msgs posearray message. This has been hand-crafted from the corresponding
 * geometry_msgs message file.
 * 
 * @author Juan Jesús Roldán (jj.roldan@upm.es)
 */

namespace ROSBridgeLib {
	namespace swarmcity_msgs {
		public class SensorArrayMsg : ROSBridgeMsg {
            public float _time;
            public SensorMsg[] _sensors;

			public SensorArrayMsg(JSONNode msg) {
                _time = float.Parse(msg["time"]);
                
                _sensors = new SensorMsg[msg["sensors"].Count];

                for (int i = 0; i < _sensors.Length; i++)
                {
                    _sensors[i] = new SensorMsg(msg["sensors"][i]);
                }
            }

            public SensorArrayMsg(float time, SensorMsg[] sensors)
            {
                _time = time;

                _sensors = new SensorMsg[sensors.Length];

                for (int i = 0; i < _sensors.Length; i++)
                {
                    _sensors[i] = new SensorMsg(sensors[i].GetLaserscan(), sensors[i].GetDetections(), sensors[i].GetClimate(), sensors[i]._agentproximity);
                }
            }
			
			public static string getMessageType() {
				return "swarmcity_msgs3/SensorArray";
			}

			public override string ToString() {

                string array = "[";
                for (int i = 0; i < _sensors.Length; i++)
                {
                    SensorMsg sensor = _sensors[i];
                    array = array + sensor.ToString();
                    if (_sensors.Length - i > 1)
                        array += ",";
                }
                array += "]";

				return "swarmcity_msgs3/SensorArray [time=" + _time.ToString() + ", sensors=" + array + "]";
			}
			
			public override string ToYAMLString() {
                string array = "[";
                for (int i = 0; i < _sensors.Length; i++)
                {
                    SensorMsg sensor = _sensors[i];
                    array = array + sensor.ToYAMLString();
                    if (_sensors.Length - i > 1)
                        array += ",";
                }
                array += "]";

                return "{\"time\" : " + _time + ", \"sensors\" : " + array + "}";
			}
		}
	}
}
