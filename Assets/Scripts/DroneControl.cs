﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class DroneControl : MonoBehaviour {

	public Vector3 cLinVel;
	public Vector3 cAngVel;

    public int frequency;

    public Vector3 LinVel;
    public Vector3 AngVel;

    public Vector3 pLinVel;

    public float energy;
    public float maxEnergy;

    private Vector3 targetPose;
    private Vector3 currentPose;

    private float speed;
    private float distance;

	private Stopwatch stopwatch;

    float t0;
    float tau;
    float t;

	// Use this for initialization
	void Start () {
        
        cLinVel = new Vector3(0, 0, 0);
        cAngVel = new Vector3(0, 0, 0);

        pLinVel = new Vector3(0, 0, 0);

        LinVel = new Vector3(0, 0, 0);
        AngVel = new Vector3(0, 0, 0);

        currentPose = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        targetPose = new Vector3(currentPose.x, currentPose.y, currentPose.z);

        energy = maxEnergy;

		stopwatch = new Stopwatch ();

		stopwatch.Start ();

        t0 = Time.realtimeSinceStartup;

        tau = 2;
	}
	
	// Update is called once per frame
	void Update()
	{
        if (stopwatch.Elapsed.Milliseconds > frequency)
        {
			stopwatch.Stop ();

            if (cAngVel.z == -100)
            {
                currentPose = transform.position;

                t = Time.realtimeSinceStartup;

                LinVel.x = (1 - Mathf.Exp(-(t - t0) / tau)) * cLinVel.x + Mathf.Exp(-(t - t0) / tau) * pLinVel.x;
                LinVel.y = (1 - Mathf.Exp(-(t - t0) / tau)) * cLinVel.y + Mathf.Exp(-(t - t0) / tau) * pLinVel.y;
                LinVel.z = (1 - Mathf.Exp(-(t - t0) / tau)) * cLinVel.z + Mathf.Exp(-(t - t0) / tau) * pLinVel.z;

                speed = Mathf.Sqrt(Mathf.Pow(LinVel.x, 2) + Mathf.Pow(LinVel.y, 2) + Mathf.Pow(LinVel.z, 2));

                pLinVel = LinVel;

                this.transform.Translate(LinVel * (t - t0));

                AngVel = new Vector3(0, cAngVel.y * 180 / Mathf.PI, 0);

                this.transform.Rotate(AngVel * (t - t0));
            }
            else
            {
                currentPose = transform.position;
                targetPose = cAngVel;

                speed = Mathf.Sqrt(Mathf.Pow(cLinVel.x, 2) + Mathf.Pow(cLinVel.y, 2) + Mathf.Pow(cLinVel.z, 2));
                distance = Mathf.Sqrt(Mathf.Pow(targetPose.x - currentPose.x, 2) + Mathf.Pow(targetPose.y - currentPose.y, 2) + Mathf.Pow(targetPose.z - currentPose.z, 2)) + 0.01f;

                //UnityEngine.Debug.Log("speed: " + speed);
                //UnityEngine.Debug.Log("distance: " + distance);

                speed = speed * (1 - Mathf.Exp(-distance / 4));

                //UnityEngine.Debug.Log("ModSpeed: " + speed);

                LinVel.x = speed / distance * (targetPose.x - currentPose.x);
                LinVel.y = speed / distance * (targetPose.y - currentPose.y);
                LinVel.z = speed / distance * (targetPose.z - currentPose.z);

                t = Time.realtimeSinceStartup;

                LinVel.x = (1 - Mathf.Exp(-(t - t0) / tau)) * LinVel.x + Mathf.Exp(-(t - t0) / tau) * pLinVel.x;
                LinVel.y = (1 - Mathf.Exp(-(t - t0) / tau)) * LinVel.y + Mathf.Exp(-(t - t0) / tau) * pLinVel.y;
                LinVel.z = (1 - Mathf.Exp(-(t - t0) / tau)) * LinVel.z + Mathf.Exp(-(t - t0) / tau) * pLinVel.z;

                pLinVel = LinVel;

                this.transform.Translate(LinVel * (t - t0));
            }

            bool charge = false;

            for (int i = 1; i < 6; i++)
            {
                if (Vector3.Distance(GameObject.Find("ChargeStation" + i.ToString()).transform.position, new Vector3(this.transform.position.x, 0, this.transform.position.z)) < 5.0)
                {
                    charge = true;
                }
            }

            if (charge)
            {
                energy += maxEnergy * (t - t0) / 60;

                if (energy > 1000)
                {
                    energy = 1000;
                }
            }
            else
            {
                energy -= (Mathf.Sqrt(Mathf.Sqrt(1+0.25f*Mathf.Pow((float)speed,4)) - 0.5f*Mathf.Pow((float)speed,2)) + 0.002f*Mathf.Pow(Mathf.Sqrt(1+0.25f*Mathf.Pow((float)speed,4)) + 0.5f*Mathf.Pow(speed,2), 1.5f) + 0.35f*(1+5*Mathf.Pow((float)speed,2)*0.0025f)) * (t - t0);

                //UnityEngine.Debug.Log("Speed: " + speed + " Energy: " + (Mathf.Sqrt(Mathf.Sqrt(1 + 0.25f * Mathf.Pow((float)speed, 4)) - 0.5f * Mathf.Pow((float)speed, 2)) + 0.002f * Mathf.Pow(Mathf.Sqrt(1 + 0.25f * Mathf.Pow((float)speed, 4)) + 0.5f * Mathf.Pow(speed, 2), 1.5f) + 0.35f * (1 + 5 * Mathf.Pow((float)speed, 2) * 0.0025f)) + " Time: " + (t - t0));

                if (energy < 0)
                {
                    energy = 0;
                }
            }

			//UnityEngine.Debug.Log ("currentPose: " + currentPose + " / targetPose: " + targetPose + " / Distance: " + distance + " / Speed: " + speed + " / Velocity: " + LinVel);

            t0 = Time.realtimeSinceStartup;

			stopwatch.Start ();
		}
	}
}