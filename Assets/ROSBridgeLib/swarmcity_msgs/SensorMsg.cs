﻿using System.Collections;
using System.Text;
using SimpleJSON;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.sensor_msgs;
using UnityEngine;

/*
 * Define a swarmcity_msg SensorMsg message. 
 * 
 * @author Juan Jesús Roldán (jj.roldan@upm.es)
 */

namespace ROSBridgeLib {
	namespace swarmcity_msgs {
		public class SensorMsg : ROSBridgeMsg {
            public LaserScanMsg _laserscan;
			public DetectionMsg[] _detections;
            public ClimateMsg _climate;
            public AgentProximityMsg[] _agentproximity;

            public SensorMsg(JSONNode msg) 
            {
                _laserscan = new LaserScanMsg(msg["laserscan"]);
                _detections = new DetectionMsg[msg["detections"].Count];

                for (int i = 0; i < _detections.Length; i++)
                {
                    _detections[i] = new DetectionMsg(msg["detections"][i]);
                }

                _climate = new ClimateMsg(msg["climate"]);
                _agentproximity = new AgentProximityMsg[msg["agentproximity"].Count];

                for (int i = 0; i < _agentproximity.Length; i++)
                {
                    _agentproximity[i] = new AgentProximityMsg(msg["agentproximity"][i]);
                }
            }

            public SensorMsg(LaserScanMsg laserscan, DetectionMsg[] detections, ClimateMsg climate, AgentProximityMsg[] agentproximity)
            {
                _laserscan = laserscan;

                _detections = new DetectionMsg[detections.Length];

                for (int i = 0; i < detections.Length; i++)
                {
                    _detections[i] = new DetectionMsg(detections[i].GetType(), detections[i].GetFeatures(), detections[i].GetPosition());
                }

                _climate = climate;

                _agentproximity = new AgentProximityMsg[agentproximity.Length];

                for (int i = 0; i < agentproximity.Length; i++)
                {
                    _agentproximity[i] = new AgentProximityMsg(agentproximity[i]._ID, agentproximity[i]._x_m, agentproximity[i]._y_m, agentproximity[i]._z_m);
                }
            }

            public LaserScanMsg GetLaserscan()
            {
                return _laserscan;
            }

            public DetectionMsg[] GetDetections()
            {
                return _detections;
            }

            public ClimateMsg GetClimate()
            {
                return _climate;
            }

			public static string getMessageType() {
				return "swarmcity_msgs2/Sensor";
			}
			
			public override string ToString() {
                string array = "[";
                for (int i = 0; i < _detections.Length; i++)
                {
                    DetectionMsg detection = _detections[i];
                    array = array + detection.ToString();
                    if (_detections.Length - i > 1)
                        array += ",";
                }
                array += "]";

                string arraya = "[";
                for (int i = 0; i < _agentproximity.Length; i++)
                {
                    AgentProximityMsg agentproximity = _agentproximity[i];
                    arraya = arraya + agentproximity.ToString();
                    if (_agentproximity.Length - i > 1)
                        arraya += ",";
                }
                arraya += "]";

                return "swarmcity_msgs2/Sensor [laserscan=" + _laserscan.ToString() + ",  detections=" + array + ", climate=" + _climate.ToString() + ", agentproximity=" + arraya + "]";
			}
			
			public override string ToYAMLString() {
                string array = "[";
                for (int i = 0; i < _detections.Length; i++)
                {
                    DetectionMsg detection = _detections[i];
                    array = array + detection.ToYAMLString();
                    if (_detections.Length - i > 1)
                        array += ",";
                }
                array += "]";

                string arraya = "[";
                for (int i = 0; i < _agentproximity.Length; i++)
                {
                    AgentProximityMsg agentproximity = _agentproximity[i];
                    arraya = arraya + agentproximity.ToYAMLString();
                    if (_agentproximity.Length - i > 1)
                        arraya += ",";
                }
                arraya += "]";

                return "{\"laserscan\" : " + _laserscan.ToYAMLString() + ", \"detections\" : " + array + ", \"climate\" : " + _climate.ToYAMLString() + ", \"agentproximity\" : " + arraya + "}";
			}
		}
	}
}
