﻿using System.Collections;
using System.Text;
using SimpleJSON;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.geometry_msgs;
using ROSBridgeLib.sensor_msgs;
using ROSBridgeLib.swarmcity_msgs;
using UnityEngine;

/*
 * Define a swarmcity_msgs Car message. This has been hand-crafted from the corresponding
 * geometry_msgs message file.
 * 
 * @author Juan Jesús Roldán (jj.roldan@upm.es)
 */

namespace ROSBridgeLib {
	namespace swarmcity_msgs {
		public class PedestrianArrayMsg : ROSBridgeMsg {

            public PedestrianMsg[] _pedestrians;

            public PedestrianArrayMsg(JSONNode msg) {

                _pedestrians = new PedestrianMsg[msg["pedestrians"].Count];

                for (int i = 0; i < _pedestrians.Length; i++)
                {
                    _pedestrians[i] = new PedestrianMsg(msg["pedestrians"][i]);
                }
            }

            public PedestrianArrayMsg(PedestrianMsg[] pedestrians)
            {
                _pedestrians = pedestrians;
            }
			
			public static string getMessageType() {
				return "swarmcity_msgs2/PedestrianArray";
			}
			
			public override string ToString() {
                string array = "[";
                for (int i = 0; i < _pedestrians.Length; i++)
                {
                    array = array + "\"" + _pedestrians[i].ToString() + "\"";
                    if (_pedestrians.Length - i > 1)
                        array += ",";
                }
                array += "]";

                return "swarmcity_msgs2/PedestrianArray [pedestrians=\"" + array + "]";
			}
			
			public override string ToYAMLString() {
                string array = "[";
                for (int i = 0; i < _pedestrians.Length; i++)
                {
                    array = array + _pedestrians[i].ToYAMLString();
                    if (_pedestrians.Length - i > 1)
                        array += ",";
                }
                array += "]";

                return "{\"pedestrians\" : " + array + "}";
			}
		}
	}
}
