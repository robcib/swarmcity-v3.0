﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Climate
{
    public int station;
    public int day;
    public double Tmed;
    public double Tmin;
    public double Tmax;
    public double Rain;
}

