﻿using ROSBridgeLib;
using ROSBridgeLib.std_msgs;
using System.Collections;
using SimpleJSON;
using UnityEngine;

/*
 * Publisher of Float32 message.
 * 
 * @author Juan Jesús Roldán (jj.roldan@upm.es)
 */

public class Float32Pub : ROSBridgeSubscriber
{

    public new static string GetMessageTopic()
    {
        return "/Swarm/Time";
    }

    public new static string GetMessageType()
    {
        return "std_msgs/Float32";
    }

    public static string ToString(Float32Msg msg)
    {
        return msg.ToString();
    }

    public static string ToYAMLString(Float32Msg msg)
    {
        return msg.ToYAMLString();
    }

}
