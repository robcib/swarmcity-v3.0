﻿using System.Collections;
using System.Text;
using SimpleJSON;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.geometry_msgs;
using ROSBridgeLib.sensor_msgs;
using UnityEngine;

/*
 * Define a swarmcity_msgs Detection message. This has been hand-crafted from the corresponding
 * geometry_msgs message file.
 * 
 * @author Juan Jesús Roldán (jj.roldan@upm.es)
 */

namespace ROSBridgeLib {
	namespace swarmcity_msgs {
		public class DetectionMsg : ROSBridgeMsg {
            private string _type;
            private string[] _features;
            private ROSBridgeLib.geometry_msgs.PoseMsg _pose;
            //private PointMsg _position;

            public DetectionMsg(JSONNode msg) {
                _type = msg["laserscan"];
                _features = new string[msg["camera"].Count];

                for (int i = 0; i < _features.Length; i++)
                {
                    _features[i] = msg["features"][i];
                }

                _pose = new ROSBridgeLib.geometry_msgs.PoseMsg(msg["position"]);
            }

            public DetectionMsg(string type, string[] features, ROSBridgeLib.geometry_msgs.PoseMsg pose)
            {
                _type = type;
                _features = features;
                _pose = pose;
            }

            public string GetType()
            {
                return _type;
            }

            public string[] GetFeatures()
            {
                return _features;
            }

            public ROSBridgeLib.geometry_msgs.PoseMsg GetPosition()
            {
                return _pose;
            }
			
			public static string getMessageType() {
				return "swarmcity_msgs1/Detection";
			}
			
			public override string ToString() {
                string array = "[";
                for (int i = 0; i < _features.Length; i++)
                {
                    array = array + "\"" + _features[i] + "\"";
                    if (_features.Length - i > 1)
                        array += ",";
                }
                array += "]";

                return "swarmcity_msgs1/Detection [type=\"" + _type + "\",  features=" + array + ", position=" + _pose.ToString() + "]";
			}
			
			public override string ToYAMLString() {
                string array = "[";
                for (int i = 0; i < _features.Length; i++)
                {
                    array = array + "\"" + _features[i] + "\"";
                    if (_features.Length - i > 1)
                        array += ",";
                }
                array += "]";

                return "{\"type\" : \"" + _type + "\", \"features\" : " + array + ", \"position\" : " + _pose.ToYAMLString() + "}";
			}
		}
	}
}
