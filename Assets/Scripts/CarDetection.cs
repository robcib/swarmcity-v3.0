﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    string Type;
    string Color;
    Vector2 Position;

    public Car()
    {
        Type = "";
        Color = "";
        Position = new Vector2(0, 0);
    }

    public Car(string type, string color, Vector2 position)
    {
        Type = type;
        Color = color;
        Position = position;
    }
}

public class CarDetection : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}
}
