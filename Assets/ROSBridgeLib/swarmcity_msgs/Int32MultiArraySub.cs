﻿using ROSBridgeLib;
using ROSBridgeLib.std_msgs;
using System.Collections;
using SimpleJSON;
using UnityEngine;

/*
 * Publisher of Int32MultiArray message.
 * 
 * @author Juan Jesús Roldán (jj.roldan@upm.es)
 */

public class Int32MultiArraySub : ROSBridgeSubscriber {

	public new static string GetMessageTopic() {
		return "/Swarm/Mode";
	}  

	public new static string GetMessageType() {
		return "std_msgs/Int32MultiArray";
	}

    public static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new Int32MultiArrayMsg(msg);
    }

    public static void CallBack(ROSBridgeMsg msg)
    {
        Int32MultiArrayMsg mode_msg = (Int32MultiArrayMsg)msg;

        int[] modes = mode_msg.GetData();

        for (int i = 0; i < modes.Length; i++)
        {
            GameObject drone = GameObject.Find("Drone" + (i+1).ToString());

            DronePayload payload = drone.GetComponentInChildren<DronePayload>();

            payload.mode = modes[i];
        }
    }
}
