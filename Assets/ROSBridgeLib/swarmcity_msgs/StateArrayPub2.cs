﻿using ROSBridgeLib;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.geometry_msgs;
using ROSBridgeLib.swarmcity_msgs;
using System.Collections;
using SimpleJSON;
using UnityEngine;

/*
 * Publisher of StateArray message.
 * 
 * @author Juan Jesús Roldán (jj.roldan@upm.es)
 */

public class StateArrayPub2 : ROSBridgePublisher
{

    public new static string GetMessageTopic()
    {
        return "/Swarm/StateArray2";
    }

    public new static string GetMessageType()
    {
        return "swarmcity_msgs1/StateArray";
    }

    public static string ToString(StateArrayMsg msg)
    {
        return msg.ToString();
    }

    public static string ToYAMLString(StateArrayMsg msg)
    {
        return msg.ToYAMLString();
    }
}
