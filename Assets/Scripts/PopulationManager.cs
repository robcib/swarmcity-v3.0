﻿using PopulationEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using System.IO;

public class PopulationManager : MonoBehaviour {

    public double frequency;

    TimeGenerator tg;

    private Stopwatch stopwatch;

    string msg;

    int Stadium0;
    int Stadium1;
    int Stadium2;
    int Industry1;
    int Industry2;
    int Industry3;
    int Industry4;
    int Park1;
    int Park2;
    int Park3;
    int Station1;
    int Station2;
    int Centre1;
    int Centre2;
    int Centre3;
    int Centre4;
    int Residence1;
    int Residence2;
    int Residence3;
    int Residence4;

    public int previousHour;
    int open;

    public float match_time;
    public float timeUp;
    public bool match;
    public double temperature;

    public double carbon_oxides, sulfur_oxides, nitrogen_oxides, particles, particles1, particles2, particles3, particles4;

    private ClimateManager climate;

    // Use this for initialization
    void Start () {

        tg = GameObject.Find("Time").GetComponent<TimeGenerator>();

        foreach (GameObject go in GameObject.FindGameObjectsWithTag("Population"))
        {
            PopulationGenerator pg = go.GetComponent<PopulationGenerator>();

            pg.enabled = true;
        }
        
        stopwatch = new Stopwatch();

        stopwatch.Start();

        Stadium0 = 0;
        Stadium1 = 0;
        Stadium2 = 0;
        Industry1 = 0;
        Industry2 = 0;
        Industry3 = 0;
        Industry4 = 0;
        Park1 = 0;
        Park2 = 0;
        Park3 = 0;
        Station1 = 0;
        Station2 = 0;
        Centre1 = 0;
        Centre2 = 0;
        Centre3 = 0;
        Centre4 = 0;
        Residence1 = 0;
        Residence2 = 0;
        Residence3 = 0;
        Residence4 = 0;

        msg = "Hora; Stadium; Stadium1; Stadium2; Industry1; Industry2; Industry3; Industry4; Park1; Park2; Park3; Station1; Station2; Centre1; Centr2; Centre3; Centre4; Residence1; Residence2; Residence 3; Residence4; Temperature; Sulfure; Carbon; Nitrogen; Particles; \n";

        previousHour = tg.date.Hour;
        open = 1;

        match = false;

        climate = GameObject.Find("Climate").GetComponent<ClimateManager>();
        temperature = 0;
    }
	
	// Update is called once per frame
	void Update () {

        if (stopwatch.Elapsed.Seconds > frequency / 1000)
        {
            stopwatch.Stop();

            foreach (GameObject go in GameObject.FindGameObjectsWithTag("Population"))
            {
                PopulationGenerator pg = go.GetComponent<PopulationGenerator>();

                /*if(tg.date.Hour % 3 == 0)
                {
                    if (pg.enabled == false) pg.enabled = true;
                }*/

                switch(pg.name)
                {
                    case "Stadium1":
                        if (tg.date.Hour > previousHour)
                        {
                            for (int i = 0; i < go.transform.childCount; i++)
                            {
                                Transform child = go.transform.GetChild(i);

                                if (child.tag == "Person")
                                {
                                    Stadium1++;
                                }
                            }
                        }
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Sunday)||(tg.date.DayOfWeek == System.DayOfWeek.Saturday)) && ((tg.date.Hour > 17) && (tg.date.Hour < 22)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 20;
                                pg.ManualGeneration();
                            }
                        }
                        else 
                        {
                            if (pg.enabled == true)
                            {
                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                                pg.totalToSpawn = 10;
                                pg.ManualGeneration();

                                pg.enabled = false;
                            }
                        }
                        
                        break;

                    case "Stadium2":
                        if (tg.date.Hour > previousHour)
                        {
                            for (int i = 0; i < go.transform.childCount; i++)
                            {
                                Transform child = go.transform.GetChild(i);

                                if (child.tag == "Person")
                                {
                                    Stadium2++;
                                }
                            }
                        }
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Sunday) || (tg.date.DayOfWeek == System.DayOfWeek.Saturday)) && ((tg.date.Hour > 17) && (tg.date.Hour < 22)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 25;
                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                                pg.totalToSpawn = 12;
                                pg.ManualGeneration();

                                pg.enabled = false;
                            }
                        }
                        
                        break;

                    case "Industry1":
                        if (tg.date.Hour > previousHour)
                        {
                            for (int i = 0; i < go.transform.childCount; i++)
                            {
                                Transform child = go.transform.GetChild(i);

                                if (child.tag == "Person")
                                {
                                    Industry1++;
                                }
                            }
                        }
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 8) && (tg.date.Hour < 13)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 20;
                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                                pg.totalToSpawn = 10;
                                pg.ManualGeneration();

                                pg.enabled = false;
                            }
                        }
                        
                        break;

                    case "Industry2":
                        if (tg.date.Hour > previousHour)
                        {
                            for (int i = 0; i < go.transform.childCount; i++)
                            {
                                Transform child = go.transform.GetChild(i);

                                if (child.tag == "Person")
                                {
                                    Industry2++;
                                }
                            }
                        }
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 8) && (tg.date.Hour < 13)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 20;
                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                                pg.totalToSpawn = 10;
                                pg.ManualGeneration();

                                pg.enabled = false;
                            }
                        }
                        
                        break;

                    case "Industry3":
                        if (tg.date.Hour > previousHour)
                        {
                            for (int i = 0; i < go.transform.childCount; i++)
                            {
                                Transform child = go.transform.GetChild(i);

                                if (child.tag == "Person")
                                {
                                    Industry3++;
                                }
                            }
                        }
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 16) && (tg.date.Hour < 20)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 30;
                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                                pg.totalToSpawn = 15;
                                pg.ManualGeneration();

                                pg.enabled = false;
                            }
                        }
                        
                        break;

                    case "Industry4":
                        if (tg.date.Hour > previousHour)
                        {
                            for (int i = 0; i < go.transform.childCount; i++)
                            {
                                Transform child = go.transform.GetChild(i);

                                if (child.tag == "Person")
                                {
                                    Industry4++;
                                }
                            }
                        }
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 16) && (tg.date.Hour < 20)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 20;
                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                                pg.totalToSpawn = 10;
                                pg.ManualGeneration();

                                pg.enabled = false;
                            }
                        }
                        
                        break;

                    case "Park1":
                        if (tg.date.Hour > previousHour)
                        {
                            for (int i = 0; i < go.transform.childCount; i++)
                            {
                                Transform child = go.transform.GetChild(i);

                                if (child.tag == "Person")
                                {
                                    Park1++;
                                }
                            }
                        }
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 17) && (tg.date.Hour < 21)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 40;
                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Sunday) || (tg.date.DayOfWeek == System.DayOfWeek.Saturday)) && ((tg.date.Hour > 9) && (tg.date.Hour < 21)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 40;
                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                                pg.totalToSpawn = 15;
                                pg.ManualGeneration();

                                pg.enabled = false;
                            }
                        }
                        
                        break;

                    case "Park2":
                        if (tg.date.Hour > previousHour)
                        {
                            for (int i = 0; i < go.transform.childCount; i++)
                            {
                                Transform child = go.transform.GetChild(i);

                                if (child.tag == "Person")
                                {
                                    Park2++;
                                }
                            }
                        }
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 17) && (tg.date.Hour < 21)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 40;
                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Sunday) || (tg.date.DayOfWeek == System.DayOfWeek.Saturday)) && ((tg.date.Hour > 9) && (tg.date.Hour < 21)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 40;
                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                                pg.totalToSpawn = 15;
                                pg.ManualGeneration();

                                pg.enabled = false;
                            }
                        }
                        
                        break;

                    case "Park3":
                        if (tg.date.Hour > previousHour)
                        {
                            for (int i = 0; i < go.transform.childCount; i++)
                            {
                                Transform child = go.transform.GetChild(i);

                                if (child.tag == "Person")
                                {
                                    Park3++;
                                }
                            }
                        }
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 17) && (tg.date.Hour < 21)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 40;
                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Sunday) || (tg.date.DayOfWeek == System.DayOfWeek.Saturday)) && ((tg.date.Hour > 9) && (tg.date.Hour < 21)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 40;
                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                                pg.totalToSpawn = 15;
                                pg.ManualGeneration();

                                pg.enabled = false;
                            }
                        }
                        
                        break;

                    case "Station1":
                        if (tg.date.Hour > previousHour)
                        {
                            for (int i = 0; i < go.transform.childCount; i++)
                            {
                                Transform child = go.transform.GetChild(i);

                                if (child.tag == "Person")
                                {
                                    Station1++;
                                }
                            }
                        }
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 6) && (tg.date.Hour < 9)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 20;
                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                                pg.totalToSpawn = 10;
                                pg.ManualGeneration();

                                pg.enabled = false;
                            }
                        }
                        
                        break;

                    case "Station2":
                        if (tg.date.Hour > previousHour)
                        {
                            for (int i = 0; i < go.transform.childCount; i++)
                            {
                                Transform child = go.transform.GetChild(i);

                                if (child.tag == "Person")
                                {
                                    Station2++;
                                }
                            }
                        }
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 19) && (tg.date.Hour < 22)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 20;
                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                                pg.totalToSpawn = 10;
                                pg.ManualGeneration();

                                pg.enabled = false;
                            }
                        }
                        
                        break;

                    case "Centre1":
                        if (tg.date.Hour > previousHour)
                        {
                            for (int i = 0; i < go.transform.childCount; i++)
                            {
                                Transform child = go.transform.GetChild(i);

                                if (child.tag == "Person")
                                {
                                    Centre1++;
                                }
                            }
                        }
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 13) && (tg.date.Hour < 15)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 20;
                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 19) && (tg.date.Hour < 21)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 20;
                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Saturday) || (tg.date.DayOfWeek == System.DayOfWeek.Sunday)) && ((tg.date.Hour > 9) && (tg.date.Hour < 14)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 20;
                                pg.ManualGeneration();
                            }
                        }
                        else                        
                        {
                            if (pg.enabled == true)
                            {
                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                                pg.totalToSpawn = 10;
                                pg.ManualGeneration();

                                pg.enabled = false;
                            }
                        }
                        
                        break;

                    case "Centre2":
                        if (tg.date.Hour > previousHour)
                        {
                            for (int i = 0; i < go.transform.childCount; i++)
                            {
                                Transform child = go.transform.GetChild(i);

                                if (child.tag == "Person")
                                {
                                    Centre2++;
                                }
                            }
                        }
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 13) && (tg.date.Hour < 15)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 20;
                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 19) && (tg.date.Hour < 21)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 20;
                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Saturday) || (tg.date.DayOfWeek == System.DayOfWeek.Sunday)) && ((tg.date.Hour > 9) && (tg.date.Hour < 14)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 20;
                                pg.ManualGeneration();
                            }
                        }
                        else                        
                        {
                            if (pg.enabled == true)
                            {
                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                                pg.totalToSpawn = 10;
                                pg.ManualGeneration();

                                pg.enabled = false;
                            }
                        }
                        
                        break;

                    case "Centre3":
                        if (tg.date.Hour > previousHour)
                        {
                            for (int i = 0; i < go.transform.childCount; i++)
                            {
                                Transform child = go.transform.GetChild(i);

                                if (child.tag == "Person")
                                {
                                    Centre3++;
                                }
                            }
                        }
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 13) && (tg.date.Hour < 15)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 20;
                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 19) && (tg.date.Hour < 21)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 20;
                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Saturday) || (tg.date.DayOfWeek == System.DayOfWeek.Sunday)) && ((tg.date.Hour > 9) && (tg.date.Hour < 14)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 20;
                                pg.ManualGeneration();
                            }
                        }
                        else                        
                        {
                            if (pg.enabled == true)
                            {
                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                                pg.totalToSpawn = 10;
                                pg.ManualGeneration();

                                pg.enabled = false;
                            }
                        }
                        
                        break;

                    case "Centre4":
                        if (tg.date.Hour > previousHour)
                        {
                            for (int i = 0; i < go.transform.childCount; i++)
                            {
                                Transform child = go.transform.GetChild(i);

                                if (child.tag == "Person")
                                {
                                    Centre4++;
                                }
                            }
                        }
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 13) && (tg.date.Hour < 15)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 20;
                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 19) && (tg.date.Hour < 21)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 20;
                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Saturday) || (tg.date.DayOfWeek == System.DayOfWeek.Sunday)) && ((tg.date.Hour > 9) && (tg.date.Hour < 14)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 20;
                                pg.ManualGeneration();
                            }
                        }
                        else                        
                        {
                            if (pg.enabled == true)
                            {
                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                                pg.totalToSpawn = 10;
                                pg.ManualGeneration();

                                pg.enabled = false;
                            }
                        }
                        
                        break;

                    case "Residence1":
                        if (tg.date.Hour > previousHour)
                        {
                            for (int i = 0; i < go.transform.childCount; i++)
                            {
                                Transform child = go.transform.GetChild(i);

                                if (child.tag == "Person")
                                {
                                    Residence1++;
                                }
                            }
                        }
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 7) && (tg.date.Hour < 12)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 30;
                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 18) && (tg.date.Hour < 22)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 30;
                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Saturday) || (tg.date.DayOfWeek == System.DayOfWeek.Sunday)) && ((tg.date.Hour > 10) && (tg.date.Hour < 14) || (tg.date.Hour > 19) && (tg.date.Hour < 22)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 30;
                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                                pg.totalToSpawn = 10;
                                pg.ManualGeneration();

                                pg.enabled = false;
                            }
                        }
                        
                        break;

                    case "Residence2":
                        if (tg.date.Hour > previousHour)
                        {
                            for (int i = 0; i < go.transform.childCount; i++)
                            {
                                Transform child = go.transform.GetChild(i);

                                if (child.tag == "Person")
                                {
                                    Residence2++;
                                }
                            }
                        }
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 7) && (tg.date.Hour < 12)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 30;
                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 18) && (tg.date.Hour < 22)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 30;
                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Saturday) || (tg.date.DayOfWeek == System.DayOfWeek.Sunday)) && ((tg.date.Hour > 10) && (tg.date.Hour < 14) || (tg.date.Hour > 19) && (tg.date.Hour < 22)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 30;
                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }

                                pg.totalToSpawn = 10;
                                pg.ManualGeneration();

                                pg.enabled = false;
                            }
                        }
                        
                        break;

                    case "Residence3":
                        if (tg.date.Hour > previousHour)
                        {
                            for (int i = 0; i < go.transform.childCount; i++)
                            {
                                Transform child = go.transform.GetChild(i);

                                if (child.tag == "Person")
                                {
                                    Residence3++;
                                }
                            }
                        }
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 7) && (tg.date.Hour < 12)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 30;
                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 18) && (tg.date.Hour < 22)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 30;
                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Saturday) || (tg.date.DayOfWeek == System.DayOfWeek.Sunday)) && ((tg.date.Hour > 10) && (tg.date.Hour < 14) || (tg.date.Hour > 19) && (tg.date.Hour < 22)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 30;
                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                                pg.totalToSpawn = 10;
                                pg.ManualGeneration();

                                pg.enabled = false;
                            }
                        }
                        
                        break;

                    case "Residence4":
                        if (tg.date.Hour > previousHour)
                        {
                            for (int i = 0; i < go.transform.childCount; i++)
                            {
                                Transform child = go.transform.GetChild(i);

                                if (child.tag == "Person")
                                {
                                    Residence4++;
                                }
                            }
                        }
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 7) && (tg.date.Hour < 12)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;
                                
                                pg.totalToSpawn = 30;
                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 18) && (tg.date.Hour < 22)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 30;
                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Saturday) || (tg.date.DayOfWeek == System.DayOfWeek.Sunday)) && ((tg.date.Hour > 10) && (tg.date.Hour < 14)|| (tg.date.Hour > 19) && (tg.date.Hour < 22)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.totalToSpawn = 30;
                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                                pg.totalToSpawn = 10;
                                pg.ManualGeneration();

                                pg.enabled = false;
                            }
                        }
                        
                        break;

                }
            }

            if ((match == false) && (Time.realtimeSinceStartup - timeUp > match_time))
            {
                GameObject stadium = GameObject.Find("Stadium");

                PopulationGenerator pg = stadium.GetComponent<PopulationGenerator>();

                if (pg.enabled == false)
                {
                    pg.enabled = true;
             
                    pg.ManualGeneration();
                    timeUp = Time.realtimeSinceStartup;
                }
                
                for (int i = 0; i < stadium.transform.childCount; i++)
                {
                    Transform child = stadium.transform.GetChild(i);

                    if (child.tag == "Person")
                    {
                        Stadium0++;
                    }
                }
            }
            if ((match == true) && (Time.realtimeSinceStartup - timeUp > 10))
            {
                GameObject stadium = GameObject.Find("Stadium");
                PopulationGenerator pg = stadium.GetComponent<PopulationGenerator>();
                for (int i = 0; i < stadium.transform.childCount; i++)
                {
                    Transform child = stadium.transform.GetChild(i);

                    if (child.tag == "Person")
                    {
                        Destroy((child as Transform).gameObject);
                    }
                }
                timeUp = Time.realtimeSinceStartup;
            }

            if (tg.date.Hour == 0)
            {
                previousHour = 0;
            }

            if (tg.date.Hour > previousHour)
            {
                particles = climate.climate.GetTemperature(new Vector3(0,0,0), tg.date.Hour, climate.anomaly, climate.anomaly_pose);

                particles1 = climate.pollution.GetParticles(new Vector3(-147, 10, 136), tg.date.Hour, climate.anomaly, climate.anomaly_pose);

                particles2 = climate.pollution.GetParticles(new Vector3(-161, 10, -149), tg.date.Hour, climate.anomaly, climate.anomaly_pose);

                particles3 = climate.pollution.GetParticles(new Vector3(-11, 10, -108), tg.date.Hour, climate.anomaly, climate.anomaly_pose);

                particles4 = climate.pollution.GetParticles(new Vector3(183, 10, -192), tg.date.Hour, climate.anomaly, climate.anomaly_pose);

                msg = msg + tg.date.Hour + ";" + Stadium0 + ";" + Stadium1 + ";" + Stadium2 + ";" + Industry1 + ";" + Industry2 + ";" + Industry3 + ";" + Industry4 + ";" + Park1 + ";" + Park2 + ";" + Park3 + ";" + Station1 + ";" + Station2 + ";" + Centre1 + ";" + Centre2 + ";" + Centre3 + ";" + Centre4 + ";" + Residence1 + ";" + Residence2 + ";" + Residence3 + ";" + Residence4 + ";" + particles + ";" + particles1 + ";" + particles2 + ";" + particles3 + ";" + particles4 + ";" + "\n";

                previousHour = tg.date.Hour;

                UnityEngine.Debug.Log("Número de personas: " + msg);
                Stadium0 = 0;
                Stadium1 = 0;
                Stadium2 = 0;
                Industry1 = 0;
                Industry2 = 0;
                Industry3 = 0;
                Industry4 = 0;
                Park1 = 0;
                Park2 = 0;
                Park3 = 0;
                Station1 = 0;
                Station2 = 0;
                Centre1 = 0;
                Centre2 = 0;
                Centre3 = 0;
                Centre4 = 0;
                Residence1 = 0;
                Residence2 = 0;
                Residence3 = 0;
                Residence4 = 0;
            }
            if (tg.date.Day > 2 && open == 1)
            {
                StreamWriter strWriter = new StreamWriter(Application.streamingAssetsPath + "/Climate/Poblacion" + ".csv");
                strWriter.Write(msg);
                strWriter.Close();
                UnityEngine.Debug.Log("EXCEL CERRADO!!!");
                open = 3;
            }

            stopwatch.Start();
        }
	}
}
