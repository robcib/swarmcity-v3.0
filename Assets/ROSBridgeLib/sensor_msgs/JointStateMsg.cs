using System.Collections;
using System.Text;
using SimpleJSON;
using ROSBridgeLib.std_msgs;
using UnityEngine;

//Header header

//string[] name
//float64[] position
//float64[] velocity
//float64[] effort

namespace ROSBridgeLib {
	namespace sensor_msgs {
		public class JointStateMsg : ROSBridgeMsg {
			public HeaderMsg _header;
			public string[] _name;
			public float[] _position;
			public float[] _velocity;
			public float[] _effort;

			public JointStateMsg(JSONNode msg) {
				_header = new HeaderMsg(msg["header"]);

				_name = new string[msg["name"].Count];
				_position = new float[msg ["position"].Count];
				_velocity = new float[msg ["velocity"].Count];
				_effort = new float[msg ["effort"].Count];

				for (int i = 0; i < _name.Length; i++) {
					_name [i] = msg ["name"] [i];
					_position [i] = float.Parse(msg ["position"] [i]);
					_velocity [i] = float.Parse(msg ["velocity"] [i]);
					_effort [i] = float.Parse(msg ["effort"] [i]);
				}
			}

			public HeaderMsg GetHeader() {
				return _header;
			}

			public string[] GetName() {
				return _name;
			}

			public float[] GetPosition() {
				return _position;
			}

			public float[] GetVelocity() {
				return _velocity;
			}

			public float[] GetEffort() {
				return _effort;
			}

			public override string ToString() {
				return "JointState [header=" + _header.ToString ()
				+ ",  name=" + _name.ToString ()
				+ ",  position=" + _position.ToString ()
				+ ",  velocity=" + _velocity.ToString ()
				+ ",  effort=" + _effort.ToString ();
			}

			public override string ToYAMLString() {
				return "{\"header\" : " + _header.ToYAMLString() 
					+ ", \"name\" : " + _name.ToString ()
					+ ", \"position\" : " + _position.ToString ()
					+ ", \"velocity\" : " + _velocity.ToString ()
					+ ", \"effort\" : " + _effort.ToString () + "}";
			}
		}
	}
}
			