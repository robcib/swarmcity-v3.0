﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyCameraManager : MonoBehaviour {

    public Camera cam;
    private GameObject drone;
    void Start () {
        string name = "Drone" + ((int)Random.Range(1,10)).ToString();
        drone = GameObject.Find(name);

        if (drone)
        {
            cam.transform.position = drone.transform.position;
        }
        
	}

	void LateUpdate () {

        if (drone)
        {
            cam.transform.position = drone.transform.position;
        }

    }
}
