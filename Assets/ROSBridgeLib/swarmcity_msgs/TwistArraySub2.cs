﻿using ROSBridgeLib;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.geometry_msgs;
using ROSBridgeLib.swarmcity_msgs;
using System.Collections;
using SimpleJSON;
using UnityEngine;

/*
 * Publisher of TwistArray message.
 * 
 * @author Juan Jesús Roldán (jj.roldan@upm.es)
 */

public class TwistArraySub2 : ROSBridgeSubscriber {

	public new static string GetMessageTopic() {
		return "/Swarm/TwistArray2";
	}  

	public new static string GetMessageType() {
		return "swarmcity_msgs1/TwistArray";
	}

    public static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new TwistArrayMsg(msg);
    }

    public static void CallBack(ROSBridgeMsg msg)
    {
        //Debug.Log("TwistArrayMsg received!");

        TwistArrayMsg twistarray = (TwistArrayMsg) msg;

        //Debug.Log("MSG: " + twistarray.ToString());

        TwistMsg[] twists = twistarray.GetTwists();

        for (int i = 0; i < twists.Length; i++)
        {
            GameObject drone = GameObject.Find("Drone" + (i+1).ToString());

            DroneControl control = drone.GetComponentInChildren<DroneControl>();

            control.cLinVel = new Vector3((float)twists[i].GetLinear().GetX(), (float)twists[i].GetLinear().GetZ(), (float)twists[i].GetLinear().GetY());
            control.cAngVel = new Vector3((float)twists[i].GetAngular().GetX(), (float)twists[i].GetAngular().GetZ(), (float)twists[i].GetAngular().GetY());
        }

        //Debug.Log("TwistArrayMsg processed!");
    }
}
