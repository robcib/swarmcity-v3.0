﻿using System.Collections;
using System.Text;
using SimpleJSON;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.geometry_msgs;
using ROSBridgeLib.sensor_msgs;
using UnityEngine;

/*
 * Define a swarmcity_msgs Car message. This has been hand-crafted from the corresponding
 * geometry_msgs message file.
 * 
 * @author Juan Jesús Roldán (jj.roldan@upm.es)
 */

namespace ROSBridgeLib {
	namespace swarmcity_msgs {
		public class CarMsg : ROSBridgeMsg {
            public string _name;
            public string[] _type;
            public ROSBridgeLib.geometry_msgs.PoseMsg _pose;
            public double _speed;
            public int _detected;
            //private PointMsg _position;

            public CarMsg(JSONNode msg) {

                _name = msg["name"];

                _type = new string[msg["type"].Count];

                for (int i = 0; i < _type.Length; i++)
                {
                    _type[i] = msg["type"][i];
                }

                _pose = new ROSBridgeLib.geometry_msgs.PoseMsg(msg["pose"]);

                _speed = float.Parse(msg["speed"]);

                _detected = int.Parse(msg["detected"]);
            }

            public CarMsg(string name, string[] type, ROSBridgeLib.geometry_msgs.PoseMsg pose, float speed, int detected)
            {
                _name = name;
                _type = type;
                _pose = pose;
                _speed = speed;
                _detected = detected;
            }
			
			public static string getMessageType() {
				return "swarmcity_msgs1/Car";
			}
			
			public override string ToString() {
                string array = "[";
                for (int i = 0; i < _type.Length; i++)
                {
                    array = array + "\"" + _type[i] + "\"";
                    if (_type.Length - i > 1)
                        array += ",";
                }
                array += "]";

                return "swarmcity_msgs1/Car [name=\"" + _name + "\",  type=" + array + ", pose=" + _pose.ToString() + ", speed=" + _speed.ToString() + ", detected=" + _detected.ToString() + "]";
			}
			
			public override string ToYAMLString() {
                string array = "[";
                for (int i = 0; i < _type.Length; i++)
                {
                    array = array + "\"" + _type[i] + "\"";
                    if (_type.Length - i > 1)
                        array += ",";
                }
                array += "]";

                return "{\"name\" : \"" + _name + "\", \"type\" : " + array + ", \"pose\" : " + _pose.ToYAMLString() + ", \"speed\" : " + _speed.ToString() + ", \"detected\" : " + _detected.ToString() + "}";
			}
		}
	}
}
