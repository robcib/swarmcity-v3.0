﻿using ROSBridgeLib;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.geometry_msgs;
using ROSBridgeLib.swarmcity_msgs;
using System.Collections;
using SimpleJSON;
using UnityEngine;

/*
 * Publisher of CarArray message.
 * 
 * @author Juan Jesús Roldán (jj.roldan@upm.es)
 */

public class CarArrayPub : ROSBridgePublisher {

	public new static string GetMessageTopic() {
		return "/City/CarArray";
	}  

	public new static string GetMessageType() {
		return "swarmcity_msgs2/CarArray";
	}

    public static string ToString(CarArrayMsg msg)
    {
		return msg.ToString ();
	}

    public static string ToYAMLString(CarArrayMsg msg)
    {
		return msg.ToYAMLString ();
	}
}
