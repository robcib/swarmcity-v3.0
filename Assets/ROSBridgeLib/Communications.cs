﻿using UnityEngine;
using ROSBridgeLib;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.geometry_msgs;
using ROSBridgeLib.sensor_msgs;
using ROSBridgeLib.swarmcity_msgs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Globalization;
//using Valve.VR.InteractionSystem;

public class Communications : MonoBehaviour {

    public float poseFrequency, sensorFrequency, carFrequency, peopleFrequency, temperatureFrequency, particlesFrequency;

	private float time, time_pose, time_sensor, time_car, time_people, time_temperature, time_particles;

    private int pose_ind, sensor_ind;

    private ROSBridgeWebSocketConnection ros = null;

    public string IP;

    void Start () {

        Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");

        string text = System.IO.File.ReadAllLines(Application.streamingAssetsPath + "/Rules.txt")[0];

        IP = text.Split('-')[2];

        //UnityEngine.Debug.Log(IP);

        ros = new ROSBridgeWebSocketConnection ("ws://" + IP, 9090);

        ros.AddSubscriber (typeof(TwistArraySub));

        ros.AddPublisher(typeof(StateArrayPub));
        ros.AddPublisher(typeof(SensorArrayPub));

        ros.AddPublisher(typeof(CarArrayPub));
        ros.AddPublisher(typeof(PedestrianArrayPub));
        ros.AddPublisher(typeof(TemperaturePub));
        ros.AddPublisher(typeof(ParticlesPub));

        ros.Connect();

		time = 0; 
		time_pose = 0;
        time_sensor = 0;
        time_car = 0;
        time_people = 0;
        time_temperature = 0;
        time_particles = 0;

        pose_ind = 0;
        sensor_ind = 0;
    }

    void OnApplicationQuit()
    {
        if (ros != null)
        {
            ros.Disconnect();
        }
    }

    // Update is called once per frame in Unity
    void Update()
    {
        time = Time.realtimeSinceStartup;

		if ((time - time_pose) * 1000 > poseFrequency) {

            PublishStates();

            time_pose = Time.realtimeSinceStartup;

            //UnityEngine.Debug.Log("PublishStates()");
        }

        if ((time - time_sensor) * 1000 > sensorFrequency) {

            PublishSensors();

            time_sensor = Time.realtimeSinceStartup;

            //UnityEngine.Debug.Log("PublishSensors()");
        }

        if ((time - time_car) * 1000 > carFrequency)
        {
            PublishCars();

            time_car = Time.realtimeSinceStartup;
        }

        if ((time - time_people) * 1000 > peopleFrequency)
        {
            PublishPedestrians();

            time_people = Time.realtimeSinceStartup;
        }

        if ((time - time_temperature) * 1000 > temperatureFrequency)
        {
            PublishTemperature();

            time_temperature = Time.realtimeSinceStartup;
        }

        if ((time - time_particles) * 1000 > particlesFrequency)
        {
            PublishParticles();

            time_particles = Time.realtimeSinceStartup;
        }

        ros.Render ();
    }

    private void PublishStates()
    {
        pose_ind++;

        HeaderMsg header = new HeaderMsg(pose_ind, new TimeMsg((int)time, (int)(1000 * time)), "Swarm");

        GameObject[] drones = GameObject.FindGameObjectsWithTag("Drone");

        ROSBridgeLib.geometry_msgs.PoseMsg[] poses = new ROSBridgeLib.geometry_msgs.PoseMsg[drones.Length];

        ROSBridgeLib.geometry_msgs.TwistMsg[] twists = new ROSBridgeLib.geometry_msgs.TwistMsg[drones.Length];

        float[] energies = new float[drones.Length];

        for (int i = 0; i < drones.Length; i++)
        //foreach (GameObject drone in drones)
        {
            string name = "Drone" + (i+1).ToString();

            //UnityEngine.Debug.Log("NAME:" + name);

            GameObject drone = GameObject.Find(name);

            //UnityEngine.Debug.Log(drone.name);

            Vector3 position = new Vector3();
            position = drone.transform.position;

            Quaternion rotation = new Quaternion();
            rotation = drone.transform.localRotation;

            ROSBridgeLib.geometry_msgs.PoseMsg pose = new ROSBridgeLib.geometry_msgs.PoseMsg(new PointMsg((float)position.x, (float)position.z, (float)position.y), new QuaternionMsg((float)rotation.x, (float)rotation.z, (float)rotation.y, (float)rotation.w));

            poses[i] = pose;

            Vector3 linVel = new Vector3();
            linVel = drone.GetComponent<DroneControl>().LinVel;

            Vector3 angVel = new Vector3();
            angVel = drone.GetComponent<DroneControl>().AngVel;

            ROSBridgeLib.geometry_msgs.TwistMsg twist = new ROSBridgeLib.geometry_msgs.TwistMsg(new Vector3Msg((float)linVel.x, (float)linVel.y, (float)linVel.z), new Vector3Msg((float)angVel.x, (float)angVel.y, (float)angVel.z));

            twists[i] = twist;

            energies[i] = drone.GetComponent<DroneControl>().energy;
            
            //i++;
        }

        //Array.Reverse(poses);
        //Array.Reverse(twists);

        StateArrayMsg stateArray = new StateArrayMsg(Time.realtimeSinceStartup, poses, twists, energies);

        ros.Publish(StateArrayPub.GetMessageTopic(), stateArray);

        //UnityEngine.Debug.Log("Pose msg sent!");
    }

    private void PublishSensors()
    {
        sensor_ind++;
        
        HeaderMsg header = new HeaderMsg(sensor_ind, new TimeMsg((int)time, (int)(1000 * time)), "Swarm");

        GameObject[] drones = GameObject.FindGameObjectsWithTag("Drone");

        SensorMsg[] sensors = new SensorMsg[drones.Length];
        
        for (int i = 0; i < drones.Length; i++)
        //foreach (GameObject drone in drones)
        {
            string name = "Drone" + (i+1).ToString();

//            UnityEngine.Debug.Log("NAME:" + name);

            GameObject drone = GameObject.Find(name);

  //          UnityEngine.Debug.Log(drone.name);
            DronePayload dronePayload = drone.GetComponent<DronePayload>();

            LaserScan l = dronePayload.laserscan;

            LaserScanMsg laserscan = new LaserScanMsg(header, l._angle_min, l._angle_max, l._angle_increment, l._time_increment, l._scan_time, l._range_min, l._range_max, l._ranges, l._intensities);

            //UnityEngine.Debug.Log(laserscan);

            List<Detection> d = dronePayload.detections;

            DetectionMsg[] detections = new DetectionMsg[d.Count];

            for (int j = 0; j < d.Count; j++)
            {
                //UnityEngine.Debug.Log(j);

                PointMsg point = new PointMsg(d[j]._position.x, d[j]._position.y, 0.0f);

                Quaternion q = Quaternion.identity;

                //UnityEngine.Debug.Log(d[j]._angle);

                q.eulerAngles = new Vector3(0, 0, (float)d[j]._angle);// * (float)180.0f / (float)Math.PI);

                QuaternionMsg quaternion = new QuaternionMsg(q.x, q.y, q.z, q.w);

                detections[j] = new DetectionMsg(d[j]._type, d[j]._features, new ROSBridgeLib.geometry_msgs.PoseMsg(point, quaternion));

                //UnityEngine.Debug.Log(detections[j]);
            }

            ClimateMsg climate = new ClimateMsg((float)dronePayload.temperature, 0.0f, (float)dronePayload.sulfur_oxides, (float)dronePayload.carbon_oxides, (float)dronePayload.nitrogen_oxides, (float)dronePayload.particles);

            AgentProximityMsg[] agentproximity = new AgentProximityMsg[drones.Length];
            
            for (int j = 0; j < agentproximity.Length; j++)
            {
                agentproximity[j] = new AgentProximityMsg(new int[drones.Length], new int[drones.Length], new int[drones.Length], new int[drones.Length]);
            }

            SensorMsg sensor = new SensorMsg(laserscan, detections, climate, agentproximity);

            sensors[i] = sensor;

            //i++;
        }

        //Array.Reverse(sensors);

        SensorArrayMsg sensorArray = new SensorArrayMsg(Time.realtimeSinceStartup, sensors);

        //UnityEngine.Debug.Log("MSG: " + sensorArray.ToString());

        ros.Publish(SensorArrayPub.GetMessageTopic(), sensorArray);

        //UnityEngine.Debug.Log(sensorArray);

        //UnityEngine.Debug.Log("Sensor msg sent!");
    }

    private void PublishCars()
    {
        TrafficManager tm = GameObject.FindObjectOfType<TrafficManager>();

        CarMsg[] cararray = new CarMsg[tm.numberOfCars];

        int carind = 0;

        foreach (TrafficSystemVehicle car in tm.cars)
        {
            string[] cartype = new string[2];

            switch (car.name)
            {
                case "Car0(Clone)":
                    //Debug.Log("Drone " + drone.name + " detected red car");
                    cartype = new string[2] { "car", "red" };
                    break;
                case "Car1(Clone)":
                    //Debug.Log("Drone " + drone.name + " detected blue car");
                    cartype = new string[2] { "car", "blue" };
                    break;
                case "Car2(Clone)":
                    //Debug.Log("Drone " + drone.name + " detected green car");
                    cartype = new string[2] { "car", "green" };
                    break;
                case "Car3(Clone)":
                    //Debug.Log("Drone " + drone.name + " detected purple car");
                    cartype = new string[2] { "car", "purple" };
                    break;
                case "Car4(Clone)":
                    //Debug.Log("Drone " + drone.name + " detected cyan car");
                    cartype = new string[2] { "car", "cyan" };
                    break;
                case "Car5(Clone)":
                    //Debug.Log("Drone " + drone.name + " detected orange car");
                    cartype = new string[2] { "car", "orange" };
                    break;
                case "Car6(Clone)":
                    //Debug.Log("Drone " + drone.name + " detected gray car");
                    cartype = new string[2] { "car", "gray" };
                    break;
                case "Van1(Clone)":
                    //Debug.Log("Drone " + drone.name + " detected white van");
                    cartype = new string[2] { "van", "white" };
                    break;
                case "Van2(Clone)":
                    //Debug.Log("Drone " + drone.name + " detected green van");
                    cartype = new string[2] { "van", "green" };
                    break;
            }

            Vector2 carposition = new Vector2(car.transform.position.x, car.transform.position.z);

            Quaternion carorientation = Quaternion.AngleAxis(car.transform.rotation.eulerAngles.y, new Vector3(0, 0, 1));

            ROSBridgeLib.geometry_msgs.PoseMsg carpose = new ROSBridgeLib.geometry_msgs.PoseMsg(new PointMsg(carposition.x, carposition.y, 0.0f), new QuaternionMsg(carorientation.x, carorientation.y, carorientation.z, carorientation.w));

            int cardetected = 0;

            double distance = 10;
            double probability = 1;

            GameObject[] drones = GameObject.FindGameObjectsWithTag("Drone");

            foreach(GameObject drone in drones)
            {
                Vector2 droneposition = new Vector2(drone.transform.position.x, drone.transform.position.z);

                if ((Vector2.Distance(droneposition, carposition) < distance) && (UnityEngine.Random.value < probability))
                {
                    cardetected = 1;
                }
            }

            CarMsg carmsg = new CarMsg(car.name, cartype, carpose, car.m_velocity, cardetected);

            cararray[carind] = carmsg;

            carind++;
        }

        CarArrayMsg cararraymsg = new CarArrayMsg(cararray);

        ros.Publish(CarArrayPub.GetMessageTopic(), cararraymsg);
    }

    private void PublishPedestrians()
    {
        GameObject[] people = GameObject.FindGameObjectsWithTag("Person");

        PedestrianMsg[] pedestrianarray = new PedestrianMsg[people.Length];

        int personind = 0;

        foreach (GameObject person in people)
        {
            string[] persontype = new string[2];

            switch (person.name)
            {
                case "NPC Woman 1(Clone)":
                    //Debug.Log("Drone " + drone.name + " detected red car");
                    persontype = new string[2] { "person", "woman" };
                    break;
                case "NPC Woman 2(Clone)":
                    //Debug.Log("Drone " + drone.name + " detected red car");
                    persontype = new string[2] { "person", "woman" };
                    break;
                case "NPC Woman 3(Clone)":
                    //Debug.Log("Drone " + drone.name + " detected red car");
                    persontype = new string[2] { "person", "woman" };
                    break;
                case "NPC Woman 4(Clone)":
                    //Debug.Log("Drone " + drone.name + " detected red car");
                    persontype = new string[2] { "person", "woman" };
                    break;
                case "NPC Man 1(Clone)":
                    //Debug.Log("Drone " + drone.name + " detected blue car");
                    persontype = new string[2] { "person", "man" };
                    break;
                case "NPC Man 2(Clone)":
                    //Debug.Log("Drone " + drone.name + " detected blue car");
                    persontype = new string[2] { "person", "man" };
                    break;
                case "NPC Man 3(Clone)":
                    //Debug.Log("Drone " + drone.name + " detected blue car");
                    persontype = new string[2] { "person", "man" };
                    break;
                case "NPC Man 4(Clone)":
                    //Debug.Log("Drone " + drone.name + " detected blue car");
                    persontype = new string[2] { "person", "man" };
                    break;
            }

            Vector2 carposition = new Vector2(person.transform.position.x, person.transform.position.z);

            Quaternion carorientation = Quaternion.AngleAxis(person.transform.rotation.eulerAngles.y, new Vector3(0, 0, 1));

            ROSBridgeLib.geometry_msgs.PoseMsg carpose = new ROSBridgeLib.geometry_msgs.PoseMsg(new PointMsg(carposition.x, carposition.y, 0.0f), new QuaternionMsg(carorientation.x, carorientation.y, carorientation.z, carorientation.w));

            int persondetected = 0;

            double distance = 10;
            double probability = 1;

            GameObject[] drones = GameObject.FindGameObjectsWithTag("Drone");

            foreach (GameObject drone in drones)
            {
                Vector2 droneposition = new Vector2(drone.transform.position.x, drone.transform.position.z);

                if ((Vector2.Distance(droneposition, carposition) < distance) && (UnityEngine.Random.value < probability))
                {
                    persondetected = 1;
                }
            }

            PedestrianMsg pedestrianmsg = new PedestrianMsg(person.name, persontype, carpose, 0.0f, persondetected);

            pedestrianarray[personind] = pedestrianmsg;

            personind++;
        }

        PedestrianArrayMsg pedestrianarraymsg = new PedestrianArrayMsg(pedestrianarray);

        ros.Publish(PedestrianArrayPub.GetMessageTopic(), pedestrianarraymsg);
    }


    private void PublishTemperature()
    {
        ClimateManager climate = GameObject.Find("Climate").GetComponent<ClimateManager>();

        TimeGenerator time_gen = GameObject.Find("Time").GetComponent<TimeGenerator>();

        int hour = time_gen.date.Hour;

        /*ROSBridgeLib.geometry_msgs.PoseMsg[] temperatures = new ROSBridgeLib.geometry_msgs.PoseMsg[57 * 57];

        //float init = Time.realtimeSinceStartup;

        for (int i = 0; i < 57; i++)
        {
            for (int j = 0; j < 57; j++)
            {
                float x = i * 10 - 280;
                float y = j * 10 - 280;
                float z = (float) climate.climate.GetTemperature(new Vector3(x, y, 0), hour, false, new Vector3(0, 0, 0));

                //UnityEngine.Debug.Log(i * 58 + j);

                temperatures[i * 57 + j] = new ROSBridgeLib.geometry_msgs.PoseMsg(new PointMsg(x, y, z), new QuaternionMsg(0, 0, 0, 1));
            }
        }*/

        ROSBridgeLib.geometry_msgs.PoseMsg[] temperatures = new ROSBridgeLib.geometry_msgs.PoseMsg[29 * 29];

        for (int i = 0; i < 29; i++)
        {
            for (int j = 0; j < 29; j++)
            {
                float x = i * 20 - 280;
                float y = j * 20 - 280;
                float z = (float)climate.climate.GetTemperature(new Vector3(x, y, 0), hour, false, new Vector3(0, 0, 0));

                //UnityEngine.Debug.Log(i * 58 + j);

                temperatures[i * 29 + j] = new ROSBridgeLib.geometry_msgs.PoseMsg(new PointMsg(x, y, z), new QuaternionMsg(0, 0, 0, 1));
            }
        }

        //float finish = Time.realtimeSinceStartup;

        //UnityEngine.Debug.Log(finish - init);

        //UnityEngine.Debug.Log(temperatures.Length);
        //UnityEngine.Debug.Log(temperatures[0]);
        //UnityEngine.Debug.Log(temperatures[3247]);

        HeaderMsg header = new HeaderMsg(pose_ind, new TimeMsg((int)time, (int)(1000 * time)), "City");

        PoseArrayMsg temperaturemsg = new PoseArrayMsg(header, temperatures);

        ros.Publish(TemperaturePub.GetMessageTopic(), temperaturemsg);
    }


    private void PublishParticles()
    {
        ClimateManager climate = GameObject.Find("Climate").GetComponent<ClimateManager>();

        TimeGenerator time_gen = GameObject.Find("Time").GetComponent<TimeGenerator>();

        int hour = time_gen.date.Hour;

        ROSBridgeLib.geometry_msgs.PoseMsg[] particles = new ROSBridgeLib.geometry_msgs.PoseMsg[57 * 57];

        for (int i = 0; i < 57; i++)
        {
            for (int j = 0; j < 57; j++)
            {
                float x = i * 10 - 280;
                float y = j * 10 - 280;
                float z = (float)climate.pollution.GetParticles(new Vector3(x, y, 0), hour, false, new Vector3(0, 0, 0));

                //UnityEngine.Debug.Log(i * 58 + j);

                particles[i * 57 + j] = new ROSBridgeLib.geometry_msgs.PoseMsg(new PointMsg(x, y, z), new QuaternionMsg(0, 0, 0, 1));
            }
        }

        //UnityEngine.Debug.Log(temperatures.Length);
        //UnityEngine.Debug.Log(temperatures[0]);
        //UnityEngine.Debug.Log(temperatures[3247]);

        HeaderMsg header = new HeaderMsg(pose_ind, new TimeMsg((int)time, (int)(1000 * time)), "City");

        PoseArrayMsg particlesmsg = new PoseArrayMsg(header, particles);

        ros.Publish(ParticlesPub.GetMessageTopic(), particlesmsg);
    }


    private void PublishTime()
    {
        Float32Msg time = new Float32Msg(Time.realtimeSinceStartup);

        ros.Publish(Float32Pub.GetMessageTopic(), time);
    }
}