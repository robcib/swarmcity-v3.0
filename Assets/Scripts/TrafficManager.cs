﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Text;
using System.Globalization;

public class TrafficManager : MonoBehaviour {

    public TrafficSystem trafficSystem;

    public int numberOfCars;
    public int previousNumberOfCars;
    public int l;
    public int open;
    public int[] fin;

    TimeGenerator tg;

    public CultureInfo culture = new CultureInfo("en-US");

    private TrafficSystemPiece piece;

    public TrafficSystemVehicle car0;
    public TrafficSystemVehicle car1;
    public TrafficSystemVehicle car2;
    public TrafficSystemVehicle car3;
    public TrafficSystemVehicle car4;
    public TrafficSystemVehicle car5;
    public TrafficSystemVehicle car6;

    public List<TrafficSystemVehicle> cars;
    public List<TrafficSystemVehicle> eliminateCars;
    public List<TrafficSystemVehicle> carsDetected;
    public TrafficSystemVehicle[] previousCar;
    public List<TrafficSystemVehicle> jamList;
    public List<TrafficSystemVehicle> emptyList;
    private List<string> car_names;
    public List<float> maxVelocity;
    public List<float> maxJamVelocity;

    double last;
    double lastJam;
    double startJam;
    int[] roadCarNumber;
    int previousHour;
    float speedLimit;

    public string[] msg;
    public string message;
    public string carMessage;

    public float[] startTimeDetection;
    public float[] endTimeDetection;
    public Vector3[] startPosition;
    public Vector3[] endPosition;

    public Vector3 jam_point;
    public float jam_time;
    public float jam_generator;
    public bool jam;
    public bool createJam;
    private StreamWriter strWriter;


    // Use this for initialization
    void Start()
    {
        string text = System.IO.File.ReadAllText(Application.streamingAssetsPath + "/Rules.txt");

        numberOfCars = int.Parse(text.Split('-')[1]);

        cars = new List<TrafficSystemVehicle>();
        eliminateCars = new List<TrafficSystemVehicle>();
        car_names = new List<string>();
        carsDetected = new List<TrafficSystemVehicle>();
        maxVelocity = new List<float>();
        maxJamVelocity = new List<float>();

        last = Time.realtimeSinceStartup;
        lastJam = Time.realtimeSinceStartup;

        startPosition = new Vector3[10];
        startPosition[0] = new Vector3(-208, 1, 17);
        startPosition[1] = new Vector3(-145, 1, -90);
        startPosition[2] = new Vector3(50, 1, 212);
        startPosition[3] = new Vector3(-137, 1, 190);
        startPosition[4] = new Vector3(-79, 1, 10);
        startPosition[5] = new Vector3(11, 1, -84);
        startPosition[6] = new Vector3(213, 1, -8);
        startPosition[7] = new Vector3(120, 1, -255);
        startPosition[8] = new Vector3(34, 1, -188);
        startPosition[9] = new Vector3(-54, 1, -251);

        endPosition = new Vector3[10];
        endPosition[0] = new Vector3(-220, 1, -101);
        endPosition[1] = new Vector3(-145, 1, 118);
        endPosition[2] = new Vector3(-149, 1, 202);
        endPosition[3] = new Vector3(27, 1, 190);
        endPosition[4] = new Vector3(-8, 1, 83);
        endPosition[5] = new Vector3(87, 1, -9);
        endPosition[6] = new Vector3(117, 1, -188);
        endPosition[7] = new Vector3(224, 1, 3);
        endPosition[8] = new Vector3(-35, 1, -188);
        endPosition[9] = new Vector3(-105, 1, -210);

        startTimeDetection = new float[10];
        startTimeDetection[0] = Time.realtimeSinceStartup;
        endTimeDetection = new float[10];

        previousCar = new TrafficSystemVehicle[10];
        roadCarNumber = new int[10] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        fin = new int[10] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        msg = new string[10];
        message = "";
        carMessage = "";

        open = 1;
        l = 0;
        speedLimit = 1;

        jam = false;

        tg = GameObject.Find("Time").GetComponent<TimeGenerator>();
        previousHour = tg.date.Hour;

        previousNumberOfCars = numberOfCars;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.realtimeSinceStartup > 4)
        {
            if (cars.Count == 0)
            {
                for (int i = 0; i < numberOfCars; i++)
                {
                    TrafficSystemPiece[] pieces = GameObject.FindObjectsOfType<TrafficSystemPiece>();

                    int selected = Random.Range(0, pieces.Length);

                    piece = pieces[selected];

                    int model = Random.Range(0, 7);

                    switch (model)
                    {
                        case 0:
                            piece.SpawnRandomVehicle(car0);
                            break;
                        case 1:
                            piece.SpawnRandomVehicle(car1);
                            break;
                        case 2:
                            piece.SpawnRandomVehicle(car2);
                            break;
                        case 3:
                            piece.SpawnRandomVehicle(car3);
                            break;
                        case 4:
                            piece.SpawnRandomVehicle(car4);
                            break;
                        case 5:
                            piece.SpawnRandomVehicle(car5);
                            break;
                        case 6:
                            piece.SpawnRandomVehicle(car6);
                            break;
                    }

                    cars.Add(trafficSystem.GetSpawnedVehicles()[trafficSystem.GetSpawnedVehicles().Count - 1]);
                    car_names.Add(cars[i].name);
                }

                Debug.Log("Number of cars: " + cars.Count);
            }
            else
            {
                if (Time.realtimeSinceStartup - last > 1)
                {
                    int index = 0;

                    foreach (TrafficSystemVehicle car in cars)
                    {
                        if (car == null)
                        {
                            Debug.Log("Car " + index + " removed.");

                            TrafficSystemPiece[] pieces = GameObject.FindObjectsOfType<TrafficSystemPiece>();

                            int selected = Random.Range(0, pieces.Length);

                            piece = pieces[selected];

                            while(index >= car_names.Count)
                            {
                                car_names.Add("Car2(Clone)");
                            }

                            switch (car_names[index])
                            {
                                case "Car0(Clone)":
                                    piece.SpawnRandomVehicle(car0);
                                    break;
                                case "Car1(Clone)":
                                    piece.SpawnRandomVehicle(car1);
                                    break;
                                case "Car2(Clone)":
                                    piece.SpawnRandomVehicle(car2);
                                    break;
                                case "Car3(Clone)":
                                    piece.SpawnRandomVehicle(car3);
                                    break;
                                case "Car4(Clone)":
                                    piece.SpawnRandomVehicle(car4);
                                    break;
                                case "Car5(Clone)":
                                    piece.SpawnRandomVehicle(car5);
                                    break;
                                case "Car6(Clone)":
                                    piece.SpawnRandomVehicle(car6);
                                    break;
                            }

                            cars[index] = trafficSystem.GetSpawnedVehicles()[trafficSystem.GetSpawnedVehicles().Count - 1];
                            
                            //Debug.Log("Car spawmn " + car_names[index] + " - Car linked " + cars[index].name);

                            break;

                        }

                        index++;
                    }

                    last = Time.realtimeSinceStartup;
                }
            }
        }

        if (l != 10 && open == 1)
        {
                foreach (TrafficSystemVehicle car in cars)
                {
                    if(car != null)
                {
                    if (Vector3.Distance(car.transform.position, startPosition[l]) < 1.5)
                    {
                        Debug.Log("Coche " + l + " detectado en dirección correcta con velocidad inicial " + car.m_velocityMax + " reducida en un " + speedLimit + " a " + speedLimit * car.m_velocityMax);
                        carsDetected.Add(car);
                        maxVelocity.Add(car.m_velocityMax);

                        car.GetComponent<TrafficSystemVehicle>().m_velocityMax = speedLimit * car.m_velocityMax;
                        startTimeDetection[l] = Time.realtimeSinceStartup;
                        l++;
                        break;
                    }
                }
                    
                }
        }

        if (l != 0 && open == 1)
        {
            int number = 0;
            while (number < l)
            {
                if (carsDetected[number] != null)
                {
                    if (Vector3.Distance(carsDetected[number].transform.position, endPosition[number]) < 1.5 && fin[number] == 0)
                    {
                        endTimeDetection[number] = Time.realtimeSinceStartup - startTimeDetection[number];
                        msg[number] = endTimeDetection[number].ToString();
                        Debug.Log("Coche " + number + " detectado al final del recorrido");
                        carsDetected[number].m_velocityMax = maxVelocity[number];
                        //writeCSVfile(Application.streamingAssetsPath + "/Climate/Datos" + ".csv", msg[0]);
                        fin[number] = 1;
                    }
                    else
                    {
                        if (fin[number] == 0)
                        {
                            carsDetected[number].m_velocityMax = maxVelocity[number] * speedLimit;
                        }
                    }
                    if (fin[0] == 1 && fin[1] == 1 && fin[2] == 1 && fin[3] == 1 && fin[4] == 1 && fin[5] == 1 && fin[6] == 1 && fin[7] == 1 && fin[8] == 1 && fin[9] == 1)
                    {
                        message = message + tg.date.DayOfWeek + ";" + System.Convert.ToString(tg.date.Hour, culture) + ":" + System.Convert.ToString(tg.date.Minute, culture) + ";" + System.Convert.ToString(msg[0], culture) + ";" + System.Convert.ToString(msg[1], culture) + ";" + System.Convert.ToString(msg[2], culture) + ";" + System.Convert.ToString(msg[3], culture) + ";" + System.Convert.ToString(msg[4], culture) + ";" + System.Convert.ToString(msg[5], culture) + ";" + System.Convert.ToString(msg[6], culture) + ";" + System.Convert.ToString(msg[7], culture) + ";" + System.Convert.ToString(msg[8], culture) + ";" + System.Convert.ToString(msg[9], culture) + ";" + "\n";
                        //writeCSVfile(Application.streamingAssetsPath + "/Climate/Datos" + ".csv", message);

                        Debug.Log(message);
                        carsDetected = new List<TrafficSystemVehicle>();
                        maxVelocity = new List<float>();
                        fin = new int[10] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                        l = 0;
                    }
                }
                else
                {
                    fin[number] = 1;
                    msg[number] = "0";
                }
                number++;
            }
        }
        if (tg.date.Day > 1 && open == 1)
        {
            StreamWriter strWriter = new StreamWriter(Application.streamingAssetsPath + "/Climate/Datos" + ".csv");
            strWriter.Write(message);
            strWriter.Close();
            Debug.Log("EXCEL CERRADO!!!");
            StreamWriter carWriter = new StreamWriter(Application.streamingAssetsPath + "/Climate/Datos2" + ".csv");
            carWriter.Write(carMessage);
            carWriter.Close();
            open = 3;
        }

        if (jam == false && ((Time.realtimeSinceStartup - startJam) > jam_generator))
        {
            JamPoint();
            startJam = Time.realtimeSinceStartup;
            
            jam = true;
            
            Jam();
        }



        if (jam == true && (Time.realtimeSinceStartup - startJam) > jam_time)
        {
            jam = false;
            lastJam = Time.realtimeSinceStartup;
            int count = 0;
            foreach (TrafficSystemVehicle car in jamList)
            {
                if (car != null)
                {
                    if (maxJamVelocity[count] != null)
                    {
                        car.GetComponent<TrafficSystemVehicle>().m_velocityMax = maxJamVelocity[count] * speedLimit;
                    }
                    else car.GetComponent<TrafficSystemVehicle>().m_velocityMax = 3 * speedLimit;
                    car.GetComponent<TrafficSystemVehicle>().m_enableWaitingDestroyTimer = true;
                }
                count++;
            }
            count = 0;
            jamList = new List<TrafficSystemVehicle>();
            maxJamVelocity = new List<float>();
        }



        foreach (TrafficSystemVehicle car in cars)
        {
            if (car != null)
            {
                for (int d = 0; d < 10; d++)
                {
                    if (previousCar[d] != car)
                    {
                        if (Vector3.Distance(car.transform.position, startPosition[d]) < 1.5)
                        {

                            previousCar[d] = car;
                            roadCarNumber[d] = roadCarNumber[d] + 1;
                            Debug.Log("Numero de coches sobre la carretera " + d + ":" + roadCarNumber[d]);
                            break;
                        }

                    }
                }
            }            
        }

        if(tg.date.Hour > previousHour)
        {
            carMessage = carMessage + System.Convert.ToString(tg.date.Hour, culture) + ";" + System.Convert.ToString(roadCarNumber[0], culture) + ";" + System.Convert.ToString(roadCarNumber[1], culture) + ";" + System.Convert.ToString(roadCarNumber[2], culture) + ";" + System.Convert.ToString(roadCarNumber[3], culture) + ";" + System.Convert.ToString(roadCarNumber[4], culture) + ";" + System.Convert.ToString(roadCarNumber[5], culture) + ";" + System.Convert.ToString(roadCarNumber[6], culture) + ";" + System.Convert.ToString(roadCarNumber[7], culture) + ";" + System.Convert.ToString(roadCarNumber[8], culture) + ";" + System.Convert.ToString(roadCarNumber[9], culture) + ";" + "\n";
            roadCarNumber = new int[10] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            Debug.Log(carMessage);
            previousHour = tg.date.Hour;

            if ((tg.date.Hour > 6 && tg.date.Hour < 10))
            {
                speedLimit = speedLimit * 0.75f;
                numberOfCars = 250;
                jam_generator = 25000;
            }
            else if (tg.date.Hour > 16 && tg.date.Hour < 19)
            {
                speedLimit = speedLimit * 0.85f;
                numberOfCars = 250;
                jam_generator = 25000;
            }
            else if (tg.date.Hour > 9 && tg.date.Hour < 17)
            {
                speedLimit = 0.7f;
                numberOfCars = 150;
                jam_generator = 50000;
            }
            else
            {
                speedLimit = 1;
                numberOfCars = 100;
                jam_generator = 100000;
            }

            

            if (previousNumberOfCars != numberOfCars)
            {
                if (numberOfCars > previousNumberOfCars)
                {
                    for(int i = previousNumberOfCars; i < numberOfCars; i++)
                    {
                        cars.Add(null);
                        int aleatorio = Random.Range(0, previousNumberOfCars);
                        if (cars[aleatorio] != null)
                        {
                            car_names.Add(cars[aleatorio].name);
                        }
                        else
                        {
                            car_names.Add("Car1(Clone)");
                        }
                    }
                }
                else
                {
                    car_names = new List<string>();
                    int counting = 0;

                    foreach (TrafficSystemVehicle car in cars)
                    {
                        if (car != null)
                        {
                            if (carsDetected.Count == 0)
                            {
                                if (counting < numberOfCars)
                                {
                                    eliminateCars.Add(car);
                                    car_names.Add(car.name);
                                }
                                else
                                {
                                    car.Kill();
                                }
                            }
                            else
                            {
                                for (int i = 0; i < carsDetected.Count; i++)
                                {

                                    if (car == carsDetected[i])
                                    {
                                        eliminateCars.Add(car);
                                        car_names.Add(car.name);
                                        counting--;
                                        break;
                                    }
                                    else if (counting < (numberOfCars - carsDetected.Count))
                                    {
                                        eliminateCars.Add(car);
                                        car_names.Add(car.name);
                                        break;
                                    }
                                    else
                                    {
                                        car.Kill();
                                    }
                                }
                            }
                            counting++;

                        }
                        
                    }

                    cars = new List<TrafficSystemVehicle>();
                    cars = eliminateCars;
                    eliminateCars = new List<TrafficSystemVehicle>();
                }
                previousNumberOfCars = numberOfCars;
            }

        }


        



        /*if (jam == false)
        {
            foreach (TrafficSystemVehicle car in cars)
            {
                if (car.CrashDetected)
                {
                    Jam();

                    jam = true;
                }
            }
        }*/
    }
















    private void JamPoint()
    {
        int index = 0;
        index = Random.Range(0, cars.Count);
        if (cars[index] != null)
        {
            jam_point = cars[index].transform.position;
            Debug.Log("Traffic jam provocated by car" + index.ToString());
        }
        else jam_point = new Vector3(0, 0, 0);
    }

    private void Jam()
    {
        int index = 0;
        //double min_dis = 1000000;
        double min_dis = 20;
        int min_ind = 0;

        foreach (TrafficSystemVehicle car in cars)
        {
            if (car != null)
            {
                if (Vector3.Distance(car.transform.position, jam_point) < min_dis)
                //if (car.CrashDetected)
                {
                    //min_dis = Vector3.Distance(car.transform.position, jam_point);
                    //min_ind = index;
                    maxJamVelocity.Add(car.m_velocityMax);

                    car.GetComponent<TrafficSystemVehicle>().m_velocityMax = 0;
                    car.GetComponent<TrafficSystemVehicle>().m_enableWaitingDestroyTimer = false;
                    Debug.Log("Traffic jam! Car " + index.ToString() + " stopped");
                    jamList.Add(car);
                    
                }

                index++;
            }

        }
        //cars[min_ind].GetComponent<TrafficSystemVehicle>().m_velocityMax = 0;
        //cars[min_ind].GetComponent<TrafficSystemVehicle>().m_enableWaitingDestroyTimer = false;

        //Debug.Log("Traffic jam! Car " + min_ind.ToString() + " stopped");

    }

  /*  public void writeCSVfile(string path, string msg){

        if (open == 0)
        {
            StreamWriter strWriter = new StreamWriter(path);
        }

        strWriter.Write(msg);
        strWriter.WriteLine("\n"+"segunda linea"+";");
        strWriter.Close();
    }*/

}
