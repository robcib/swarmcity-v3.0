﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateFleet : MonoBehaviour {

    public GameObject drone;
    public int numberOfDrones;


	// Use this for initialization
	void Start () 
    {
        string text = System.IO.File.ReadAllText(Application.streamingAssetsPath + "/Rules.txt");

        numberOfDrones = int.Parse(text.Split('-')[0]); 

        for (int i = 0; i < numberOfDrones; i++)
        {
            Vector3 location = new Vector3(0, 0, 0);
            
            switch (i % 5)
            {
                case 0:
                    location = new Vector3(-96.5f, 20.0f, -89.1f);
                    break;
                case 1:
                    location = new Vector3(137.6f, 20.0f, -175.9f);
                    break;
                case 2:
                    location = new Vector3(15.9f, 20.0f, 15.2f);
                    break;
                case 3:
                    location = new Vector3(-117.3f, 20.0f, 154.3f);
                    break;
                case 4:
                    location = new Vector3(140.3f, 20.0f, 73.6f);
                    break;
            }

            switch (i / 5)
            {
                case 0:
                    location.x += 1;
                    location.z += 1;
                    break;
                case 1: 
                    location.x += 1;
                    location.z -= 1;
                    break;
                case 2:
                    location.x -= 1;
                    location.z -= 1;
                    break;
                case 3:
                    location.x -= 1;
                    location.z += 1;
                    break;
                case 4:
                    location.x += 2.5f;
                    location.z += 1;
                    break;
                case 5:                    
                    location.x += 1;
                    location.z += 2.5f;
                    break;
                case 6:
                    location.x += 2.5f;
                    location.z -= 1;
                    break;
                case 7:
                    location.x += 1;
                    location.z -= 2.5f;
                    break;
                case 8:
                    location.x -= 2.5f;
                    location.z -= 1;
                    break;
                case 9:                    
                    location.x -= 1;
                    location.z -= 2.5f;
                    break;
                case 10:
                    location.x -= 2.5f;
                    location.z += 1;
                    break;
                case 11:
                    location.x -= 1;
                    location.z += 2.5f;
                    break;
            }

            GameObject clone = Instantiate(drone, location, Quaternion.Euler(0.0f, 0.0f, 0.0f));

            clone.name = "Drone" + (i+1).ToString();
            clone.transform.SetParent(this.transform);

            Transform model = clone.transform.Find("Drone");

            Transform[] list = model.GetComponentsInChildren<Transform>();

            Material material = new Material(Shader.Find("Transparent/Diffuse"));
            material.color = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));

            for (int j = 1; j < list.Length; j++)
            {
                list[j].gameObject.GetComponent<MeshRenderer>().material = material;
            }
        }
	}

    // Update is called once per frame
    void Update()
    {
		
	}
}
