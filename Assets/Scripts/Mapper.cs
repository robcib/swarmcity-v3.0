﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Timers;
using UnityEngine;

public class Mapper : MonoBehaviour {

	public int X_min, X_max;

	public int X_i;

	public int Z_min, Z_max;

	public int Z_i;

	public int Y_sample;

	public int Resolution;

	//public GameObject checker;

	public byte[,] map;

	private int work;

	// Use this for initialization
	void Start () {
		map = new byte[(Z_max - Z_min) / Resolution + 1, (X_max - X_min) / Resolution + 1];

		for (int j = Z_min; j <= Z_max; j += Resolution) {
			for (int i = X_min; i <= X_max; i += Resolution) {
				map [(j - Z_min) / Resolution, (i - X_min) / Resolution] = 0;
			}
		}

		X_i = X_min;
		Z_i = Z_min;

		work = 1;

		/*Timer timer = new Timer ();
		timer.Elapsed += new ElapsedEventHandler (onMapping);
		timer.Interval = 1;
		timer.Start ();*/
	}

	/*IEnumerator Mapping()
	{
		Debug.Log ("Mapping...");

		for (int Z_i = Z_min; Z_i <= Z_max; Z_i += Resolution) {
			for (int X_i = X_min; X_i <= X_max; X_i += Resolution) {

				//transform.position = new Vector3 (X_i, Y_sample, Z_i);

				Vector3 target = new Vector3 (X_i, Y_sample, Z_i);

				transform.Translate((target - transform.position) * Time.deltaTime);

				yield return null;

				Debug.Log ("X: " + X_i + " Z: " + Z_i + " Value: " + map [(Z_i - Z_min) / Resolution, (X_i - X_min) / Resolution] );
			}
		}

		var sr = File.CreateText ("Map.txt");

		sr.Write ("Map = [");

		for (int j = Z_min; j <= Z_max; j += Resolution) {
			for (int i = X_min; i <= X_max; i += Resolution) {
				sr.Write (map [(j - Z_min) / Resolution, (i - X_min) / Resolution].ToString ());
				sr.Write (" ");
				//map_exp [(i - X_min) * (Z_max - Z_min) / (Resolution * Resolution) + (j - Z_min) / Resolution] = map [(i - X_min) / Resolution, (j - Z_min) / Resolution];
			}
			if (j != Z_max) {
				sr.Write ("; ");
			} else {
				sr.Write ("];");
			}
		}

		sr.Close ();

		Debug.Log ("Map created!");

		yield return null;
	}*/

	/*void onMapping(object source, ElapsedEventArgs e)
	{
		if (work == true) {
			transform.position = new Vector3 (X_i, Y_sample, Z_i);

			Debug.Log ("X: " + X_i + " Z: " + Z_i + " Value: " + map [(X_i - X_min) / Resolution, (Z_i - Z_min) / Resolution]);

			Z_i += Resolution;

			if (Z_i > Z_max) {
				Z_i = Z_min;

				X_i += Resolution;
				if (X_i > X_max) {					
					work = false;

					Debug.Log ("Map created!");
				}
			}
		} else {
			var sr = File.CreateText ("MyFile.txt");

			sr.Write ("Map = [");
			for (int j = Z_min; j <= Z_max; j += Resolution){
				for (int i = X_min; i <= X_max; i += Resolution) {
					sr.Write (map [(j - Z_min) / Resolution, (i - X_min) / Resolution].ToString ());
					sr.Write (" ");
				}
			
				if (j != Z_max) {
					sr.Write ("; ");
				} else {
					sr.Write ("];");
				}
			}

			sr.Close ();
		}
	}*/
	
	// Update is called once per frame
	void Update () {

		/*if (work == true) {
			StartCoroutine ("Mapping");
			work = false;
		}*/
		if (work == 1) {
			//transform.position = new Vector3 (X_i, Y_sample, Z_i);

			Vector3 target = new Vector3 (X_i, Y_sample, Z_i);

			transform.Translate((target - transform.position));

			//Debug.Log ("X: " + X_i + " Z: " + Z_i + " Value: " + map [(Z_i - Z_min) / Resolution, (X_i - X_min) / Resolution]);

			Z_i += Resolution;

			if (Z_i > Z_max) {
				Z_i = Z_min;

				X_i += Resolution;
				if (X_i > X_max) {
					Z_i = Z_max;
					X_i = X_max;
					work++;
				}
			}
		} else if (work == 2) {
			var sr = File.CreateText ("Mapping.txt");

			sr.Write ("Map = [");
			for (int j = Z_min; j <= Z_max; j += Resolution){
				for (int i = X_min; i <= X_max; i += Resolution) {
					sr.Write (map [(j - Z_min) / Resolution, (i - X_min) / Resolution].ToString ());
					sr.Write (" ");
				}

				if (j != Z_max) {
					sr.Write ("; ");
				} else {
					sr.Write ("];");
				}
			}

			sr.Close ();

			Debug.Log ("Map created!");

			work++;
		}
	}

	/*void OnCollisionEnter(Collision collisionInfo)
	{
		int X = (int)collisionInfo.transform.position.x;
		int Z = (int)collisionInfo.transform.position.z;

		map [(Z - Z_min) / Resolution, (X - X_min) / Resolution] = byte.MaxValue;

		Debug.Log ("ENTER: X: " + X + " Z: " + Z + " Value: " + map [(Z - Z_min) / Resolution, (X - X_min) / Resolution] );	
		
		map [(Z_i - Z_min) / Resolution, (X_i - X_min) / Resolution] = byte.MaxValue;

		Debug.Log ("ENTER: X: " + X_i + " Z: " + Z_i + " Value: " + map [(Z_i - Z_min) / Resolution, (X_i - X_min) / Resolution] );
	}

	void OnCollisionStay(Collision collisionInfo)
	{
		int X = (int)collisionInfo.transform.position.x;
		int Z = (int)collisionInfo.transform.position.z;

		map [(Z - Z_min) / Resolution, (X - X_min) / Resolution] = byte.MaxValue;

		Debug.Log ("STAY: X: " + X + " Z: " + Z + " Value: " + map [(Z - Z_min) / Resolution, (X - X_min) / Resolution] );

		map [(Z_i - Z_min) / Resolution, (X_i - X_min) / Resolution] = byte.MaxValue;

		Debug.Log ("STAY: X: " + X_i + " Z: " + Z_i + " Value: " + map [(Z_i - Z_min) / Resolution, (X_i - X_min) / Resolution] );
	}

	void OnCollisionExit(Collision collisionInfo)
	{
		int X = (int)collisionInfo.transform.position.x;
		int Z = (int)collisionInfo.transform.position.z;

		map [(Z - Z_min) / Resolution, (X - X_min) / Resolution] = byte.MaxValue;

		Debug.Log ("EXIT: X: " + X + " Z: " + Z + " Value: " + map [(Z - Z_min) / Resolution, (X - X_min) / Resolution] );

		map [(Z_i - Z_min) / Resolution, (X_i - X_min) / Resolution] = byte.MaxValue;

		Debug.Log ("EXIT: X: " + X_i + " Z: " + Z_i + " Value: " + map [(Z_i - Z_min) / Resolution, (X_i - X_min) / Resolution]);
	}*/

	void OnTriggerEnter(Collider other)
	{
		/*int X = (int)collisionInfo.transform.position.x;
		int Z = (int)collisionInfo.transform.position.z;

		map [(Z - Z_min) / Resolution, (X - X_min) / Resolution] = byte.MaxValue;

		Debug.Log ("TRIGGER_ENTER: X: " + X + " Z: " + Z + " Value: " + map [(Z - Z_min) / Resolution, (X - X_min) / Resolution] );*/

		map [(Z_i - Z_min) / Resolution, (X_i - X_min) / Resolution] = byte.MaxValue;

		//Debug.Log ("ENTER: X: " + X_i + " Z: " + Z_i + " Value: " + map [(Z_i - Z_min) / Resolution, (X_i - X_min) / Resolution] );
	}

	void OnTriggerStay(Collider other)
	{
		/*int X = (int)collisionInfo.transform.position.x;
		int Z = (int)collisionInfo.transform.position.z;

		map [(Z - Z_min) / Resolution, (X - X_min) / Resolution] = byte.MaxValue;

		Debug.Log ("TRIGGER_STAY: X: " + X + " Z: " + Z + " Value: " + map [(Z - Z_min) / Resolution, (X - X_min) / Resolution] );*/

		map [(Z_i - Z_min) / Resolution, (X_i - X_min) / Resolution] = byte.MaxValue;

		//Debug.Log ("STAY: X: " + X_i + " Z: " + Z_i + " Value: " + map [(Z_i - Z_min) / Resolution, (X_i - X_min) / Resolution] );
	}

	void OnTriggerExit(Collider other)
	{
		/*int X = (int)collisionInfo.transform.position.x;
		int Z = (int)collisionInfo.transform.position.z;

		map [(Z - Z_min) / Resolution, (X - X_min) / Resolution] = byte.MaxValue;

		Debug.Log ("TRIGGER_EXIT: X: " + X + " Z: " + Z + " Value: " + map [(Z - Z_min) / Resolution, (X - X_min) / Resolution] );*/

		map [(Z_i - Z_min) / Resolution, (X_i - X_min) / Resolution] = byte.MaxValue;

		//Debug.Log ("EXIT: X: " + X_i + " Z: " + Z_i + " Value: " + map [(Z_i - Z_min) / Resolution, (X_i - X_min) / Resolution] );
	}

}
