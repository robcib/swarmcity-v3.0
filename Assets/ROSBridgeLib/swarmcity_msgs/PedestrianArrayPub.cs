﻿using ROSBridgeLib;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.geometry_msgs;
using ROSBridgeLib.swarmcity_msgs;
using System.Collections;
using SimpleJSON;
using UnityEngine;
using System.Threading;
using System.Globalization;

/*
 * Publisher of CarArray message.
 * 
 * @author Juan Jesús Roldán (jj.roldan@upm.es)
 */

public class PedestrianArrayPub : ROSBridgePublisher {

	public new static string GetMessageTopic() {
		return "/City/PedestrianArray";
	}  

	public new static string GetMessageType() {
		return "swarmcity_msgs2/PedestrianArray";
	}

    public static string ToString(CarArrayMsg msg)
    {
		return msg.ToString ();
	}

    public static string ToYAMLString(CarArrayMsg msg)
    {
        return msg.ToYAMLString ();
	}
}
