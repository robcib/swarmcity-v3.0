﻿using System.Collections;
using System.Text;
using SimpleJSON;
using ROSBridgeLib.std_msgs;
using UnityEngine;

/**
 * sensor_msgs/LaserScan.msg
 * @author Juan Jesús Roldán
 */


namespace ROSBridgeLib {
	namespace sensor_msgs {
		public class LaserScanMsg : ROSBridgeMsg {
            private HeaderMsg _header;
            private float _angle_min;
            private float _angle_max;
            private float _angle_increment;
            private float _time_increment;
            private float _scan_time;
            private float _range_min;
            private float _range_max;
            private float[] _ranges;
            private float[] _intensities;
            
			public LaserScanMsg(JSONNode msg) {
                _header = new HeaderMsg(msg["header"]);
                _angle_min = float.Parse(msg["angle_min"]);
                _angle_max = float.Parse(msg["angle_max"]);
                _angle_increment = float.Parse(msg["angle_increment"]);
                _time_increment = float.Parse(msg["time_increment"]);
                _scan_time = float.Parse(msg["scan_time"]);
                _range_min = float.Parse(msg["range_min"]);
                _range_max = float.Parse(msg["range_max"]);
                _ranges = new float[msg["ranges"].Count];
                for (int i = 0; i < _ranges.Length; i++)
                {
                    _ranges[i] = float.Parse(msg["ranges"][i]);
                }
                _intensities = new float[msg["intensities"].Count];
                for (int i = 0; i < _intensities.Length; i++)
                {
                    _intensities[i] = float.Parse(msg["intensities"][i]);
                }

			}

			public LaserScanMsg(HeaderMsg header, float angle_min, float angle_max, float angle_increment, float time_increment, float scan_time, float range_min, float range_max, float[] ranges, float[] intensities) {
                _header = header;
                _angle_min = angle_min;            
                _angle_max = angle_max;            
                _angle_increment = angle_increment;            
                _time_increment = time_increment;            
                _scan_time = scan_time;            
                _range_min = range_min;            
                _range_max = range_max;            
                _ranges = ranges;            
                _intensities = intensities;
			}

			public static string GetMessageType() {
				return "sensor_msgs/LaserScan";
			}

			public override string ToString() {
                string array_ranges = "[";
                for (int i = 0; i < _ranges.Length; i++)
                {
                    array_ranges = array_ranges + _ranges[i];
                    if (_ranges.Length - i > 1)
                        array_ranges += ",";
                }
                array_ranges += "]";

                string array_intensities = "[";
                for (int i = 0; i < _intensities.Length; i++)
                {
                    array_intensities = array_intensities + _intensities[i];
                    if (_intensities.Length - i > 1)
                        array_intensities += ",";
                }
                array_intensities += "]";

                return "sensor_msgs/LaserScan [header=" + _header.ToString() + ", angle_min=" + _angle_min + ", angle_max="
                        + _angle_max + ", angle_increment=" + _angle_increment + ", time_increment=" + _time_increment + ", scan_time=" 
                        + _scan_time + ", range_min=" + _range_min + ", range_max=" + _range_max + ", ranges=" + array_ranges + 
                        ", intensities=" + array_intensities + "]";
   			}

			public override string ToYAMLString() {
                string array_ranges = "[";
                for (int i = 0; i < _ranges.Length; i++)
                {
                    array_ranges = array_ranges + _ranges[i];
                    if (_ranges.Length - i > 1)
                        array_ranges += ",";
                }
                array_ranges += "]";

                string array_intensities = "[";
                for (int i = 0; i < _intensities.Length; i++)
                {
                    array_intensities = array_intensities + _intensities[i];
                    if (_intensities.Length - i > 1)
                        array_intensities += ",";
                }
                array_intensities += "]";

                return "{\"header\" : " + _header.ToYAMLString() + ", \"angle_min\" : " + _angle_min + ", \"angle_min\" : "
                        + _angle_max + ", \"angle_increment\" : " + _angle_increment + ", \"time_increment\" : " + _time_increment 
                        + ", \"scan_time\" : " + _scan_time + ", \"range_min\" : " + _range_min + ", \"range_max\" : " + _range_max 
                        + ", \"ranges\" : " + array_ranges + ", \"intensities\" : " + array_intensities + "}";
            }
		}
	}
}