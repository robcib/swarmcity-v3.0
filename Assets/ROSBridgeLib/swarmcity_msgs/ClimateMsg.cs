﻿using System.Collections;
using System.Text;
using SimpleJSON;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.geometry_msgs;
using ROSBridgeLib.sensor_msgs;
using UnityEngine;

/*
 * Define a swarmcity_msgs Climate message.
 * 
 * @author Juan Jesús Roldán (jj.roldan@upm.es)
 */

namespace ROSBridgeLib {
	namespace swarmcity_msgs {
		public class ClimateMsg : ROSBridgeMsg {
            private float _temperature, _rain;
            private float _sulfur_oxides, _carbon_oxides, _nitrogen_oxides, _particles;

            public ClimateMsg(JSONNode msg) {
                _temperature = float.Parse(msg["temperature"]);
                _rain = float.Parse(msg["rain"]);
                _sulfur_oxides = float.Parse(msg["sulfur_oxides"]);
                _carbon_oxides = float.Parse(msg["carbon_oxides"]);
                _nitrogen_oxides = float.Parse(msg["nitrogen_oxides"]);
                _particles = float.Parse(msg["particles"]);
            }

            public ClimateMsg(float temperature, float rain, float sulfur_oxides, float carbon_oxides, float nitrogen_oxides, float particles)
            {
                _temperature = temperature;
                _rain = rain;
                _sulfur_oxides = sulfur_oxides;
                _carbon_oxides = carbon_oxides;
                _nitrogen_oxides = nitrogen_oxides;
                _particles = particles;
            }

			public static string getMessageType() {
				return "swarmcity_msgs1/Climate";
			}
			
			public override string ToString() {
                return "swarmcity_msgs1/Climate [temperature = " + _temperature.ToString() + ",  rain = " + _rain + ", sulfur_oxides = " + _sulfur_oxides +  ",  carbon_oxides = " + _carbon_oxides + ",  nitrogen_oxides = " + _nitrogen_oxides +  ",  particles = " + _particles + "]";
			}
			
			public override string ToYAMLString() {
                return "{\"temperature\" :" + _temperature + ", \"rain\" : " + _rain + ", \"sulfur_oxides\" : " + _sulfur_oxides + ", \"carbon_oxides\" : " + _carbon_oxides + ", \"nitrogen_oxides\" : " + _nitrogen_oxides + ", \"particles\" : " + _particles + "}";
			}
		}
	}
}
