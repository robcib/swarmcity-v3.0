﻿using ROSBridgeLib;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.geometry_msgs;
using ROSBridgeLib.swarmcity_msgs;
using System.Collections;
using SimpleJSON;
using UnityEngine;

/*
 * Publisher of PoseArray message.
 * 
 * @author Juan Jesús Roldán (jj.roldan@upm.es)
 */

public class SensorArrayPub : ROSBridgePublisher {

	public new static string GetMessageTopic() {
		return "/Swarm/SensorArray";
	}  

	public new static string GetMessageType() {
		return "swarmcity_msgs3/SensorArray";
	}

    public static string ToString(SensorArrayMsg msg)
    {
		return msg.ToString ();
	}

    public static string ToYAMLString(SensorArrayMsg msg)
    {
		return msg.ToYAMLString ();
	}
}
